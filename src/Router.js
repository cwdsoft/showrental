import React from 'react';
import { Router } from 'dva/router';
import dynamic from 'dva/dynamic';
import { Spin } from 'antd';
import RouterPath from './utils/RoutePath';
import Layout from './components/layout';

Date.prototype.format = function (format) {
  let o = {
    'M+': this.getMonth() + 1, // month
    'd+': this.getDate(), // day
    'h+': this.getHours(), // hour
    'm+': this.getMinutes(), // minute
    's+': this.getSeconds(), // second
    'q+': Math.floor((this.getMonth() + 3) / 3), // quarter
    'S': this.getMilliseconds() // millisecond
  }
  if (/(y+)/.test(format)) {
    format = format.replace(RegExp.$1,
      (this.getFullYear() + '').substr(4 - RegExp.$1.length));
  }
  for (let k in o) {
    if (new RegExp('(' + k + ')').test(format)) {
      format = format.replace(RegExp.$1,
        RegExp.$1.length === 1 ? o[k] :
          ('00' + o[k]).substr(('' + o[k]).length));
    }
  }
  return format;
}

dynamic.setDefaultLoadingComponent(() => {
  return <Spin className="global-spin" />;
});

const routeConfig = [
  // {
  //   path: RouterPath.NEWSMANA,
  //   // models: () => [import('./models/tongji')],
  //   component: () => import('./routes/newsManage')
  // },
  {
    path: RouterPath.THRIRECOLOR,
    // models: () => [import('./models/tongji')],
    component: () => import('./routes/thireeColor')
  },
  {
    path: RouterPath.NEWS,
    // models: () => [import('./models/tongji')],
    component: () => import('./routes/news')
  },
  {
    path: RouterPath.SYSTEMMSG,
    // models: () => [import('./models/tongji')],
    component: () => import('./routes/systemMsg')
  },

  {
    path: RouterPath.PROBLEMROOM,
    // models: () => [import('./models/tongji')],
    component: () => import('./routes/probleRoom')
  },
  {
    path: RouterPath.PERFORMANCELOG,
    // models: () => [import('./models/tongji')],
    component: () => import('./routes/performance')
  },
  {
    path: RouterPath.LOGIN,
    // models: () => [import('./models/tongji')],
    component: () => import('./routes/login')
  },
  {
    path: RouterPath.CHECKLOG,
    // models: () => [import('./models/tongji')],
    component: () => import('./routes/checkLog')
  },
  {
    path: RouterPath.TJ,
    // models: () => [import('./models/tongji')],
    component: () => import('./routes/tongji')
  },
  {
    path: RouterPath.STRUCT,
    models: () => [import('./models/struct')],
    component: () => import('./routes/struct')
  },
  {
    path: RouterPath.FACEANLY,
    // models: () => [import('./models/common')],
    component: () => import('./routes/face')
  },
  {
    path: RouterPath.PATHANLY,
    // models: () => [import('./models/common')],
    component: () => import('./routes/trajectory')
  },
  {
    path: RouterPath.HOUSE,
    models: () => [import('./models/house')],
    component: () => import('./routes/house')
  },
  {
    path: RouterPath.PEOPLE,
    models: () => [import('./models/people')],
    component: () => import('./routes/people')
  },
  {
    path: RouterPath.VIEW,
    models: () => [import('./models/view')],
    component: () => import('./routes/view')
  },
  {
    path: RouterPath.ROOT,
    // models: () => [import('./models/view')],
    component: () => import('./routes/login')
  },
  {
    path: '*',
    // models: () => [import('./models/view')],
    component: () => import('./routes/login')
  },
]


const routes = [...routeConfig]

function RouterConfig({ history, app }) {
  const dynamicRoutes = routes.map((route) => {
    return {
      component: dynamic({
        app,
        models: route.models,
        component: route.component
      }),
      path: route.path,
      options: route.options,
      // pageId: route.pageId
    }
  });
  return (
    <div>
      <Router history={history} >
        <Layout routes={dynamicRoutes} />
      </Router>
    </div>
  );
}

export default RouterConfig;
