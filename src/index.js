import '@babel/polyfill';
import dva from 'dva';
import createLoading from 'dva-loading';
// import createBrowserHistory from 'history/createBrowserHistory';
import createHashHistory from 'history/createHashHistory';
import './index.css';

// 1. Initialize
const app = dva({
  history: createHashHistory(),
});

// 2. Plugins
app.use(createLoading());

// 3. Model
app.model(require('./models/common').default);

// 4. Router./outer
app.router(require('./Router').default);

// 5. Start
app.start('#root');
