import React, { Component } from 'react';
import { Spin } from 'antd'
import { connect } from 'dva';
// import { COMMON } from '../../utils/NameSpace';
import TreeMain from '../../components/common/treean.js'

class StructPage extends Component {

  render() {
    return (
      <div>
        <Spin spinning={this.props.loading}>
          <TreeMain />
        </Spin>
      </div>
    );
  }
}

export const mapStateToProps = (
  state
) => {
  // const { } = state[COMMON];
  const effects = state.loading.effects
  return {
    loading: !!effects['STRUCT/getTree']
    // || !!effects['STRUCT/getUserInfo']
    ,
  }
}

export default connect(mapStateToProps)(StructPage);
