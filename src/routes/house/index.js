import React, { Component } from 'react';
import { Spin } from 'antd'
import { connect } from 'dva';
// import { COMMON } from '../../utils/NameSpace';
import HouseMain from '../../components/house'

class ViewPage extends Component {

  render() {
    return (
      <div>
        <Spin spinning={this.props.loading}>
          <HouseMain />
        </Spin>
      </div>
    );
  }
}

export const mapStateToProps = (
  state
) => {
  // const { } = state[COMMON];
  const effects = state.loading.effects
  return {
    loading: !!effects['HOUSE/getUserInfo']
    || !!effects['HOUSE/getAllSuicong']
    ,
  }
}

export default connect(mapStateToProps)(ViewPage);
