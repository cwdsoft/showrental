import React, { Component } from 'react';
import { connect } from 'dva';
import { Spin } from 'antd'
// import { COMMON } from '../../utils/NameSpace';
import RichTextMain from '../../components/richText'


class NewsPage extends Component {

  render() {
    return (
      <div>
        <Spin spinning={this.props.loading}>
          <RichTextMain act="sendNews" isnews={true} />
        </Spin>
      </div>
    );
  }
}

export const mapStateToProps = (
  state
) => {
  const effects = state.loading.effects;
  return {
    loading: !!effects['COMMON/sendNews'],
  }
}

export default connect(mapStateToProps)(NewsPage);
