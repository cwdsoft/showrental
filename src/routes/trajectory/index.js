import React, { Component } from 'react';
import { Spin } from 'antd'
import { connect } from 'dva';
// import { COMMON } from '../../utils/NameSpace';
import TrajectoryMain from '../../components/common/trajectory'

class ViewPage extends Component {

  render() {
    return (
      <div>
        <Spin spinning={this.props.loading}>
          <TrajectoryMain />
        </Spin>
      </div>
    );
  }
}

export const mapStateToProps = (
  state
) => {
  // const { } = state[COMMON];
  const effects = state.loading.effects
  return {
    loading: !!effects['COMMON/getTree']
    || !!effects['COMMON/getUserPath']
    ,
  }
}

export default connect(mapStateToProps)(ViewPage);
