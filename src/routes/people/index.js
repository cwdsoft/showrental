import React, { Component } from 'react';
import { connect } from 'dva';
import { Spin } from 'antd'
// import { COMMON } from '../../utils/NameSpace';
import PeopleMain from '../../components/people'

class ViewPage extends Component {

  render() {
    console.log('people inner')
    return (
      <div>
        <Spin spinning={this.props.loading}>
          <PeopleMain />
        </Spin>
      </div>
    );
  }
}

export const mapStateToProps = (
  state
) => {
  const effects = state.loading.effects;
  return {
    loading: !!effects['PEOPLE/getInit']

    ,
  }
}

export default connect(mapStateToProps)(ViewPage);
