import React, { Component } from 'react';
import { connect } from 'dva';
import { Spin } from 'antd'
// import { COMMON } from '../../utils/NameSpace';
import NewManage from '../../components/common/newManage'


class NewsPage extends Component {

  render() {
    return (
      <div>
        <Spin spinning={this.props.loading}>
          <NewManage />
        </Spin>
      </div>
    );
  }
}

export const mapStateToProps = (
  state
) => {
  const effects = state.loading.effects;
  return {
    loading: !!effects['COMMON/sendNews'],
  }
}

export default connect(mapStateToProps)(NewsPage);
