import React, { Component } from 'react';
import { connect } from 'dva';
import { Spin } from 'antd'
// import styles from './IndexPage.css';
// import { COMMON } from '../../utils/NameSpace';
import PerformanceMain from '../../components/jixiaolog'

class PerformanceLogPage extends Component {

  render() {
    return (
      <div>
        <Spin spinning={this.props.loading}>
          <PerformanceMain />
        </Spin>
      </div>
    );
  }
}

export const mapStateToProps = (
  state
) => {
  // const { } = state[COMMON];
  const effects = state.loading.effects
  return {
    loading: !!effects['COMMON/getMjWgyTree']
    || !!effects['COMMON/getKaoheLog']
    ,
  }
}

export default connect(mapStateToProps)(PerformanceLogPage);
