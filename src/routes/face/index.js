import React, { Component } from 'react';
import { Spin } from 'antd'
import { connect } from 'dva';
// import styles from './IndexPage.css';
// import { COMMON } from '../../utils/NameSpace';
import FaceMain from '../../components/face'

class FacePage extends Component {

  render() {
    return (
      <div>
        <Spin spinning={false} >
          <FaceMain />
        </Spin>
      </div>
    );
  }
}

export const mapStateToProps = (state) => {
  // const { } = state[COMMON];
  const effects = state.loading.effects
  console.log(effects, state.loading)
  return {
    // loading: !!effects['bomCreation/getBomList'],
  }
}

export default connect(mapStateToProps)(FacePage);
