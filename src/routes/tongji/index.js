import React, { Component } from 'react';
import { connect } from 'dva';
import { Spin } from 'antd'
// import { COMMON } from '../../utils/NameSpace';
import TJMain from '../../components/tongji'

class ViewPage extends Component {

  render() {
    return (
      <div>
        <Spin spinning={this.props.loading}>
          <TJMain />
        </Spin>
      </div>
    );
  }
}

export const mapStateToProps = (
  state
) => {
  // const { } = state[COMMON];
  const effects = state.loading.effects
  return {
    loading: !!effects['COMMON/getRenPie']
    || !!effects['COMMON/getSexPie']
    ,
  }
}

export default connect(mapStateToProps)(ViewPage);
