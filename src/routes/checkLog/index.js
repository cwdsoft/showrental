import React, { Component } from 'react';
import { connect } from 'dva';
import { Spin } from 'antd'
// import { COMMON } from '../../utils/NameSpace';
import ChecklogMain from '../../components/checklog'

class CheckLogPage extends Component {

  render() {
    return (
      <div>
        <Spin spinning={this.props.loading}>
          <ChecklogMain />
        </Spin>
      </div>
    );
  }
}

export const mapStateToProps = (
  state
) => {
  // const { } = state[COMMON];
  const effects = state.loading.effects
  return {
    loading: !!effects['COMMON/getFanghaoTree']
    || !!effects['COMMON/getMjWgyTree']
    || !!effects['COMMON/getRoomCheckLog']
    ,
  }
}

export default connect(mapStateToProps)(CheckLogPage);
