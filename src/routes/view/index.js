import React, { Component } from 'react';
import { connect } from 'dva';
import { Spin } from 'antd'
// import { COMMON } from '../../utils/NameSpace';
import ViewMain from '../../components/view'

class ViewPage extends Component {

  render() {
    return (
      <div>
        <Spin spinning={this.props.loading}>
          <ViewMain />
        </Spin>
      </div>
    );
  }
}

export const mapStateToProps = (
  state
) => {
  // const { } = state[COMMON];
  const effects = state.loading.effects
  return {
    loading: !!effects['VIEW/getInit']
    || !!effects['VIEW/getJixiao']
    || !!effects['VIEW/getAllhousePosi']
    ,
  }
}

export default connect(mapStateToProps)(ViewPage);
