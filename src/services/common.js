import queryString from 'query-string';
import { GET, POST } from '../utils/Request';
import { API } from '../utils/Constant';

const {
  GetAllUserTree,
  GetAllUser,
  GetAllFangWu,
  GetAllFangHaoByFangWuId,
  GetUserInfoById,
  GetRenPie,
  GetSexPie,
  GetAllFollowUserByZKId,
  GetBaseData,
  GetUserPathById,
  GetWangGeYuanTop10,
  GetProblemRoom,
  GetAllhousePosi,
  SendNews,
  SendMsg,
  CheckLogin,
  GetFangWuFangHaoTree,
  GetMinJingWangGeTree,
  GetDailyCheckHistory,
  GetKaoHeHistoryById,
  UpdateFile,
  SearchFace,
  GetNews,
  SaveNews,
  MarkChange,
  GetFangWuList
} = API

export function getRenPie() {
  return GET(`${GetRenPie}`)
}

export function getSexPie() {
  return GET(`${GetSexPie}`)
}

export function getAllUserTree() {
  return GET(`${GetAllUserTree}`)
}

export function getBaseData() {
  return GET(`${GetBaseData}`)
}

export function getAllFangWu(queryParams) {
  return GET(`${GetAllFangWu}?${queryString.stringify(queryParams)}`)
}

export function getAllUser(queryParams) {
  return GET(`${GetAllUser}?${queryString.stringify(queryParams)}`)
}

export function getAllFangHaoByFangWuId(queryParams) {
  return GET(`${GetAllFangHaoByFangWuId}?${queryString.stringify(queryParams)}`)
}

export function getUserInfoById(queryParams) {
  return GET(`${GetUserInfoById}?${queryString.stringify(queryParams)}`)
}

export function getAllFollowUserByZKId(queryParams) {
  return GET(`${GetAllFollowUserByZKId}?${queryString.stringify(queryParams)}`)
}

export function getUserPathById(queryParams) {
  return GET(`${GetUserPathById}?${queryString.stringify(queryParams)}`)
}

export function getWangGeYuanTop10() {
  return GET(`${GetWangGeYuanTop10}`)
}

export function getProblemRoom(queryParams) {
  return GET(`${GetProblemRoom}?${queryString.stringify(queryParams)}`)
}

export function getAllhousePosi(queryParams) {
  return GET(`${GetAllhousePosi}?${queryString.stringify(queryParams)}`)
}


export function sendNews(queryParams) {
  return POST(`${SendNews}`, queryParams)
}
export function sendMsg(queryParams) {
  console.log('its me', queryParams)
  return POST(`${SendMsg}`, queryParams)
}

export function checkLogin(queryParams) {
  return POST(`${CheckLogin}`, queryParams)
}


export function getFangWuFangHaoTree() {
  return GET(`${GetFangWuFangHaoTree}`)
}

export function getFangWuList() {
  return GET(`${GetFangWuList}`)
}

export function getMinJingWangGeTree() {
  return GET(`${GetMinJingWangGeTree}`)
}

export function getDailyCheckHistory(queryParams) {
  return GET(`${GetDailyCheckHistory}?${queryString.stringify(queryParams)}`)
}

export function getKaoHeHistoryById(queryParams) {
  return GET(`${GetKaoHeHistoryById}?${queryString.stringify(queryParams)}`)
}

export function updateFile(queryParams) {
  return POST(`${UpdateFile}`, queryParams)
}

export function searchFace(queryParams) {
  return POST(`${SearchFace}`, queryParams)
}


// 公告管理
export function getNews() {
  return GET(`${GetNews}`)
}

export function markChange(queryParams) {
  return GET(`${MarkChange}?${queryString.stringify(queryParams)}`)
}

export function saveNews(queryParams) {
  return POST(`${SaveNews}`, queryParams)
}
