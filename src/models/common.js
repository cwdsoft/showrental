import queryString from 'query-string';
import { notification } from 'antd';
import { routerRedux } from 'dva/router';
import initStore from '../store/common';
import actionHelper from '../action/common'; // eslint-disable-line
import { COMMON, allName } from '../utils/NameSpace';
import * as services from '../services/common';
import path from '../utils/RoutePath'

export default {
  namespace: COMMON,
  state: initStore,
  reducers: {
    [actionHelper.SET_DATA](state, { payload }) {
      // console.log('Common SetData payload: ', payload)
      return { ...state, ...payload };
    },
  },
  effects: {
    *[actionHelper.GET_BASE_INFO]({ payload }, { call, put }) {  // eslint-disable-line
      try {
        let data = yield call(services.getBaseData);
        if (data.status === 0) {
          yield put(actionHelper.setData({
            baseInfo: data.data
          }))
        }
      } catch (e) {
        console.log(e)
      }
    },
    *[actionHelper.SEARCH_FACE]({ payload }, { call, put }) {  // eslint-disable-line
      try {
        let data = yield call(services.searchFace, payload);
        if (data.status === 0) {
          yield put(actionHelper.setData({
            searchData: data.data,
          }))
        }
      } catch (e) {
        console.log(e)
      }
    },
    *[actionHelper.GET_NEWS]({ payload }, { call, put }) {  // eslint-disable-line
      try {
        yield put(actionHelper.setData({
          newsList: [
            { key: '0-', title: '内容1', sender: '小王', date: '2019-1-1', visiable: true },
            { key: '1-', title: '内容2', sender: '小王', date: '2019-1-1', visiable: true },
            { key: '2-', title: '内容3', sender: '小王', date: '2019-1-1', visiable: true },
            { key: '3-', title: '内容4', sender: '小王', date: '2019-1-1', visiable: true },
            { key: '4-', title: '内容5', sender: '小王', date: '2019-1-1', visiable: true },
          ],
        }))

        let data = yield call(services.getNews, payload);
        if (data.status === 0) {
          yield put(actionHelper.setData({
            newsList: data.data.map((ele, i) => { ele.key = i + '-'; return ele }),
          }))
        }
      } catch (e) {
        console.log(e)
      }
    },
    *[actionHelper.SAVE_NEWS]({ payload }, { call, put }) {  // eslint-disable-line
      try {
        let data = yield call(services.saveNews, payload);
        if (data.status === 0) {
          return true
        }
        return false
      } catch (e) {
        console.log(e)
        return false
      }
    },
    *[actionHelper.UPDATE_FILE]({ payload }, { call, put }) {  // eslint-disable-line
      try {
        yield call(services.updateFile, payload);
      } catch (e) {
        console.log(e)
      }
    },
    *[actionHelper.GET_INIT]({ payload }, { call, put }) {  // eslint-disable-line
      try {
        yield put(actionHelper.setData());
      } catch (e) {
        console.log(e)
      }
    },
    *[actionHelper.GET_REN_PIE]({ payload }, { call, put }) {  // eslint-disable-line
      try {
        let data = yield call(services.getRenPie);
        if (data.status === 0) {
          yield put(actionHelper.setData({
            renPie: data.data.filter(ele => String(ele.value) !== '0'),
          }))
        }
      } catch (e) {
        console.log(e)
      }
    },
    *[actionHelper.GET_USER_INFO]({ payload }, { call, put }) {  // eslint-disable-line
      try {
        let data = yield call(services.getUserInfoById, payload);
        if (data.status === 0) {
          yield put(actionHelper.setData({
            peopleInfo: [data.data],
          }))
        }
      } catch (e) {
        console.log(e)
      }
    },
    *[actionHelper.GET_USER_PATH]({ payload }, { call, put }) {  // eslint-disable-line
      try {
        let data = yield call(services.getUserPathById, payload);
        if (data.status === 0) {
          yield put(actionHelper.setData({
            userPath: data.data || [],
          }))
        }
      } catch (e) {
        console.log(e)
      }
    },
    *[actionHelper.GET_SEX_PIE]({ payload }, { call, put }) {  // eslint-disable-line
      try {
        let data = yield call(services.getSexPie);
        if (data.status === 0) {
          yield put(actionHelper.setData({
            sexPie: data.data,
          }))
        }
      } catch (e) {
        console.log(e)
      }
    },
    *[actionHelper.GET_TREE]({ payload }, { call, put }) {  // eslint-disable-line
      try {
        let data = yield call(services.getAllUserTree);
        if (data.status === 0) {
          yield put(actionHelper.setData({
            tree: [data.data],
            showTree: [data.data],
          }))
        }
      } catch (e) {
        console.log(e)
      }
    },
    *[actionHelper.GET_ROOM_TREE]({ payload }, { call, put }) {  // eslint-disable-line
      try {
        let data = yield call(services.getAllUserTree);
        if (data.status === 0) {
          yield put(actionHelper.setData({
            roomTree: [data.data],
          }))
        }
      } catch (e) {
        console.log(e)
      }
    },
    *[actionHelper.GET_FANGHAO_TREE]({ payload }, { call, put }) {  // eslint-disable-line
      try {
        let data = yield call(services.getAllFangWu);
        if (data.status === 0) {
          yield put(actionHelper.setData({
            fangWuList: data.data,
          }))
        }
      } catch (e) {
        console.log(e)
      }
    },
    *[actionHelper.GET_MJ_WGY_TREE]({ payload }, { call, put }) {  // eslint-disable-line
      try {
        let data = yield call(services.getMinJingWangGeTree);
        if (data.status === 0) {
          yield put(actionHelper.setData({
            mjWgyTree: data.data,
          }))
        }
      } catch (e) {
        console.log(e)
      }
    },
    *[actionHelper.GET_ROOM_CHECK_LOG]({ payload }, { call, put }) {  // eslint-disable-line
      try {
        let data = yield call(services.getDailyCheckHistory, payload);
        if (data.status === 0) {
          yield put(actionHelper.setData({
            roomLogSource: data.data,
          }))
          return true
        }
        return false
      } catch (e) {
        console.log(e)
      }
    },
    *[actionHelper.GET_KAOHE_LOG]({ payload }, { call, put }) {  // eslint-disable-line
      try {
        let data = yield call(services.getKaoHeHistoryById, payload);
        if (data.status === 0) {
          yield put(actionHelper.setData({
            jixiaoLogSource: data.data,
          }))
          return true
        }
        return false
      } catch (e) {
        console.log(e)
      }
    },
    *[actionHelper.GET_PROBLEM_ROOM]({ payload }, { call, put }) {  // eslint-disable-line
      try {
        let data = yield call(services.getAllFangWu, payload);
        if (data.status === 0) {
          yield put(actionHelper.setData({
            originProblemRoomDataSource: data.data,
            problemRoomDataSource:
            data.data.filter(ele =>
              String(ele.star) === String(payload.currentStar)),
          }))
        }
      } catch (e) {
        console.log(e)
      }
    },
    *[actionHelper.SEND_NEWS]({ payload }, { call, put }) {  // eslint-disable-line
      try {
        let data = yield call(services.sendNews, payload);
        if (data.status === 0) {
          return true
        }
      } catch (e) {
        console.log(e)
      }
    },
    *[actionHelper.SEND_MSG]({ payload }, { call, put }) {  // eslint-disable-line
      try {
        let data = yield call(services.sendMsg, payload);
        if (data.status === 0) {
          return true
        }
      } catch (e) {
        console.log(e)
      }
    },
    *[actionHelper.GET_MY_INFO]({ payload }, { call, put }) {  // eslint-disable-line
      try {
        let data = yield call(services.getUserInfoById, payload);
        if (data.status === 0) {
          console.log('my info', data.data)
          yield put(actionHelper.setData({
            userInfo: data.data,
            hasUserInfo: true
          }))
        }
      } catch (e) {
        console.log(e)
      }
    },
    *[actionHelper.CHECK_LOGIN]({ payload }, { call, put }) {  // eslint-disable-line
      try {
        let data = yield call(services.checkLogin, payload);
        if (data.status === 0) {
          let storage = window.sessionStorage;
          storage.setItem('openid', data.data.openID);
          storage.setItem('ticket', 'unique123456');
          storage.setItem('userid', data.data.u.id);
          // storage.setItem('name', data.data.name);
          // storage.setItem('headpath', data.data.headpath);
          // 登陆成功保存用户信息
          yield put(actionHelper.setData({
            userInfo: data.data.u,
            hasUserInfo: true
          }))
          return true
        } else {
          notification.error({ message: '出现错误', duration: 0, description: data.errorMsg });
          return false
        }
      } catch (e) {
        console.log(e)
      }
    },
    *[actionHelper.GET_COLOR_PEOPLE]({ payload }, { call, put }) {  // eslint-disable-line
      try {
        let data = yield call(services.getAllUser, { level: 4 });
        if (data.status === 0) {
          yield put(actionHelper.setData({
            originColorPeopleDataSource: data.data,
            ColorPeopleDataSource: data.data.filter(ele =>
              String(ele.mark) === String(payload.currentPeopleClass)),
          }))
        }
        console.log('-data: ', data)
      } catch (e) {
        console.log(e)
      }
    },
  },
  subscriptions: {
    setup({ history,
      dispatch
    }) {
      return history.listen(({ pathname, search }) => {
        let query = queryString.parse(search);
        console.log('common inner', pathname, path.ROOT)
        if (pathname.indexOf(path.LOGIN) > -1 || pathname === path.ROOT) {
          console.log('login TODO')
          dispatch(actionHelper.setData({
            isLogin: true
          }))
          // dispatch(actionHelper.getRoomTree())
        } else {
          dispatch(actionHelper.setData({
            isLogin: false
          }))
        }
        if (!window.localStorage) {
          notification.open({
            message: '系统提示',
            description: '浏览器不支持localstorage.',
            style: {
              width: 600,
              marginLeft: 335 - 600,
            },
          });
        } else {
          let storage = window.sessionStorage;
          const ticket = storage.getItem('ticket');
          if ((!ticket || ticket !== 'unique123456') && pathname.indexOf(path.LOGIN) === -1) {
            notification.open({
              message: '系统提示',
              description: '您没有权限！请登录.',
              style: {
                width: 600,
                marginLeft: 335 - 600,
              },
            });
            dispatch(actionHelper.setData({
              isLogin: true
            }))
            dispatch(routerRedux.push(`${path.LOGIN}`))
            return
          }

          if (ticket && ticket === 'unique123456') {
            console.log('xxxxx')
            dispatch(actionHelper.getMyInfo({
              id: storage.getItem('userid')
            }))
            dispatch(actionHelper.setData({
              hasUserInfo: true,
            }))
          }
          // 主逻辑业务
        }
        // allName
        let s = pathname.replace('/', '')
        if (allName.includes(s)) {
          dispatch(actionHelper.setData({
            selectedKeys: s,
            isLogin: false
          }))
        }
        if (pathname.indexOf(path.PROBLEMROOM) > -1) {
          console.log('proble room TODO')
          let level = query.level
          if (!level || !['1', '2', '3', '4', '5'].includes(query.level)) {
            level = '1'
          }
          dispatch(actionHelper.setData({ currentStar: level }))
          dispatch(actionHelper.getProblemRoom({ currentStar: level }))
        }
        if (pathname.indexOf(path.THRIRECOLOR) > -1) {
          let level = query.level
          if (!level || !['严管类', '关注类', '放心类'].includes(query.level)) {
            level = '严管类'
          }
          dispatch(actionHelper.setData({ currentPeopleClass: level }))
          dispatch(actionHelper.getColorPeople({ currentPeopleClass: level }))
        }
        if (pathname.indexOf(path.FACEANLY) > -1) {
          console.log('face TODO')
        }
        if (pathname.indexOf(path.TJ) > -1) {
          console.log('face TODO')
          dispatch(actionHelper.getBaseInfo())
        }
        if (pathname.indexOf(path.CHECKLOG) > -1) {
          dispatch(actionHelper.getFanghaoTree())
          dispatch(actionHelper.getMjWgyTree())
        }
        if (pathname.indexOf(path.PERFORMANCELOG) > -1) {
          let userid = query.userid
          if (userid) {
            dispatch(actionHelper.getKaoheLog({ id: userid }))
          }
          dispatch(actionHelper.getMjWgyTree())
        }
        // if (pathname.indexOf(path.NEWSMANA) > -1) {
        //   dispatch(actionHelper.getNews())
        // }
        if (pathname.indexOf(path.TJ) > -1) {
          dispatch(actionHelper.getRenPie())
          dispatch(actionHelper.getSexPie())
        }
        if (pathname.indexOf(path.PATHANLY) > -1) {
          dispatch(actionHelper.setData({ userPath: [] }))
          dispatch(actionHelper.getTree())
          // dispatch(actionHelper.getUserPath({
          //   ZuKeId: null
          // }))
        }
      });
    }
  },
};
