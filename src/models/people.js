import queryString from 'query-string';
import { message } from 'antd';
import initStore from '../store/people';
import actionHelper from '../action/people'; // eslint-disable-line
import { PEOPLE } from '../utils/NameSpace';
import * as services from '../services/common';
import path from '../utils/RoutePath'
import { isNoData } from '../utils/Util'

export default {
  namespace: PEOPLE,
  state: initStore,
  reducers: {
    [actionHelper.SET_DATA](state, { payload }) {
      console.log('Common SetData payload: ', payload)
      return { ...state, ...payload };
    },
  },
  effects: {
    *[actionHelper.GET_INIT]({ payload }, { call, put }) {  // eslint-disable-line
      try {
        let data = yield call(services.getAllUser, payload);
        if (data.status === 0) {
          yield put(actionHelper.setData({
            dataSource: data.data,
            originDataSource: data.data
          }))
        }
        console.log('-data: ', data)
      } catch (e) {
        console.log(e)
      }
    },
    *[actionHelper.MARK_CHANGE]({ payload }, { call, put }) {  // eslint-disable-line
      try {
        let data = yield call(services.markChange, payload);
        if (data.status === 0) {
          message.success('标记成功');
        }
        console.log('-data: ', data)
      } catch (e) {
        console.log(e)
      }
    },
  },
  subscriptions: {
    setup({ history,
      dispatch
    }) {
      return history.listen(({ pathname, search }) => {
        let query = queryString.parse(search);
        let sBox = ['4', '5', '6', '7', '8', '0']
        if (pathname.indexOf(path.PEOPLE) > -1) {
          console.log('pathname： ', pathname)
          let from = !isNoData(query.from) ? query.from : null
          let to = !isNoData(query.to) ? query.to : null
          dispatch(actionHelper.setData({ startValue: from, endValue: to }))
          // currentLevel
          if (sBox.includes(query.level)) {
            dispatch(actionHelper.setData({ currentLevel: query.level }))
            dispatch(actionHelper.getInit({ level: query.level }))
            return
          }
          dispatch(actionHelper.getInit({ level: 4 }))
        }
      });
    }
  },
};

