import initStore from '../store/struct';
import actionHelper from '../action/struct'; // eslint-disable-line
import { STRUCT } from '../utils/NameSpace';
import * as services from '../services/common';
import path from '../utils/RoutePath'

export default {
  namespace: STRUCT,
  state: initStore,
  reducers: {
    [actionHelper.SET_DATA](state, { payload }) {
      console.log('Common SetData payload: ', payload)
      return { ...state, ...payload };
    },
  },
  effects: {
    *[actionHelper.GET_TREE]({ payload }, { call, put }) {  // eslint-disable-line
      try {
        let data = yield call(services.getAllUserTree);
        if (data.status === 0) {
          yield put(actionHelper.setData({
            tree: [data.data],
            showTree: [data.data],
          }))
        }
      } catch (e) {
        console.log(e)
      }
    },
    *[actionHelper.GET_USER_INFO]({ payload }, { call, put }) {  // eslint-disable-line
      try {
        let data = yield call(services.getUserInfoById, payload);
        if (data.status === 0) {
          yield put(actionHelper.setData({
            peopleInfo: data.data
          }))
        }
      } catch (e) {
        console.log(e)
      }
    }
  },
  subscriptions: {
    setup({ history, dispatch
    }) {
      return history.listen(({ pathname }) => {
        if (pathname.indexOf(path.STRUCT) > -1) {
          dispatch(actionHelper.getTree())
        }
      });
    }
  },
};

