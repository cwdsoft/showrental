import initStore from '../store/view';
import actionHelper from '../action/view'; // eslint-disable-line
import { VIEW } from '../utils/NameSpace';
import * as services from '../services/common';
import path from '../utils/RoutePath'

export default {
  namespace: VIEW,
  state: initStore,
  reducers: {
    [actionHelper.SET_DATA](state, { payload }) {
      console.log('Common SetData payload: ', payload)
      return { ...state, ...payload };
    },
  },
  effects: {
    *[actionHelper.GET_INIT]({ payload }, { call, put }) {  // eslint-disable-line
      try {
        let data = yield call(services.getBaseData);
        if (data.status === 0) {
          yield put(actionHelper.setData({
            baseInfo: data.data
          }))
        }
      } catch (e) {
        console.log(e)
      }
    },
    *[actionHelper.GET_AllHOUSE_POSI]({ payload }, { call, put }) {  // eslint-disable-line
      try {
        let data = yield call(services.getAllhousePosi);
        if (data.status === 0) {
          yield put(actionHelper.setData({
            housePosi: data.data
          }))
        }
      } catch (e) {
        console.log(e)
      }
    },
    *[actionHelper.GET_JIXIAO]({ payload }, { call, put }) {  // eslint-disable-line
      try {
        let data = yield call(services.getWangGeYuanTop10);
        if (data.status === 0) {
          yield put(actionHelper.setData({
            jixiaoRank: data.data
          }))
        }
      } catch (e) {
        console.log(e)
      }
    },
  },
  subscriptions: {
    setup({ history,
      dispatch
    }) {
      return history.listen(({ pathname }) => {
        if (pathname.indexOf(path.VIEW) > -1) {
          dispatch(actionHelper.getInit())
          dispatch(actionHelper.getJixiao())
          dispatch(actionHelper.getAllhousePosi())
        }
      });
    }
  },
};

