import queryString from 'query-string';
import initStore from '../store/house';
import actionHelper from '../action/house'; // eslint-disable-line
import { HOUSE } from '../utils/NameSpace';
import * as services from '../services/common';
import path from '../utils/RoutePath'

export default {
  namespace: HOUSE,
  state: initStore,
  reducers: {
    [actionHelper.SET_DATA](state, { payload }) {
      console.log('Common SetData payload: ', payload)
      return { ...state, ...payload };
    },
  },
  effects: {
    *[actionHelper.GET_INIT]({ payload: { searchText } }, { call, put }) {  // eslint-disable-line
      try {

        let data = yield call(services.getAllFangWu, {});
        if (data.status === 0) {
          yield put(actionHelper.setData({
            originDataSource: data.data,
            dataSource: data.data.filter((ele) => {
              let lock = false;
              if (String(ele.address).includes(searchText)) {
                lock = true;
              }
              if (String(ele.area).includes(searchText)) {
                lock = true;
              }
              if (String(ele.chuZuJianshu).includes(searchText)) {
                lock = true;
              }
              if (String(ele.yizhu).includes(searchText)) {
                lock = true;
              }
              if (String(ele.fangwuType).includes(searchText)) {
                lock = true;
              }
              if (String(ele.comment).includes(searchText)) {
                lock = true;
              }
              if (String(ele.fangdonginfo).includes(searchText)) {
                lock = true;
              }
              return lock
            })

          }))
        }
      } catch (e) {
        console.log(e)
      }
    },
    *[actionHelper.GET_ROOM]({ payload }, { call, put, select }) {  // eslint-disable-line
      try {

        let data = yield call(services.getAllFangHaoByFangWuId, payload);
        if (data.status === 0) {
          let { expendData, refresh } = yield select(state => state[HOUSE])
          yield put(actionHelper.setData({
            expendData: Object.assign(expendData, { [payload.fangwuid]: data.data }),
            refresh: !refresh
          }))
        }
      } catch (e) {
        console.log(e)
      }
    },
    *[actionHelper.GET_USER_INFO]({ payload }, { call, put }) {  // eslint-disable-line
      try {

        let data = yield call(services.getUserInfoById, payload);
        if (data.status === 0) {
          yield put(actionHelper.setData({
            peopleInfo: [data.data],
          }))
        }
      } catch (e) {
        console.log(e)
      }
    },
    *[actionHelper.GET_ALL_SUICONG]({ payload }, { call, put }) {  // eslint-disable-line
      try {

        let data = yield call(services.getAllFollowUserByZKId, payload);
        if (data.status === 0) {
          yield put(actionHelper.setData({
            suicongInfo: [data.data],
          }))
        }
      } catch (e) {
        console.log(e)
      }
    },
  },
  subscriptions: {
    setup({ history,
      dispatch
    }) {
      return history.listen(({ pathname, search }) => {
        let query = queryString.parse(search);
        if (pathname.indexOf(path.HOUSE) > -1) {
          if (query.query) {
            // dispatch(actionHelper.getInit({ fangwuid: query.fangwuid }))
            dispatch(actionHelper.setData({ searchText: query.query }))
          }
          dispatch(actionHelper.getInit({ searchText: query.query }))
        }
      });
    }
  },
};

