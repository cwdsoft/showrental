// const apiPreFix = 'http://212.64.16.204:80/api/'
export const apiPreFix = 'https://heyangpolice.club/api/'
// const apiPreFix = 'https://192.168.5.22:44337/api/'


let sAPI = {
  GetAllUserTree: `${apiPreFix}user/GetAllUserTree`,
  GetAllUser: `${apiPreFix}user/GetAllUsers`,

  GetAllFangWu: `${apiPreFix}fangwu/GetAllFangWu`,
  GetAllFangHaoByFangWuId: `${apiPreFix}fanghao/GetAllFangHaoByFWIdForWeb`,

  GetUserInfoById: `${apiPreFix}user/GetUserById`,

  GetAllFollowUserByZKId: `${apiPreFix}zuke/GetAllFollowUserByZKId`,

  // pie
  GetSexPie: `${apiPreFix}user/GetAllUserSex`,
  GetRenPie: `${apiPreFix}user/GetAllUserAge`,
  // 首页数据
  GetBaseData: `${apiPreFix}index/GetIndexData`,
  GetUserPathById: `${apiPreFix}zuke/GetZuKeMoveHistroy`,


  GetWangGeYuanTop10: `${apiPreFix}user/GetWangGeYuanTop10`,
  // 获取问题房间
  GetProblemRoom: `${apiPreFix}Fanghao/GetProblemFangHao`,

  GetAllhousePosi: `${apiPreFix}fangwu/GetAllFangWuForIndex`,

  // 发送新闻
  SendNews: `${apiPreFix}news/AddNews`,
  // 发送消息
  SendMsg: `${apiPreFix}message/AddMessage`,

  // 检查登陆信息成功
  CheckLogin: `${apiPreFix}Login/LoginForWeb`,

  GetFangWuFangHaoTree: `${apiPreFix}fanghao/GetFangWuFangHaoTree`,
  GetMinJingWangGeTree: `${apiPreFix}user/GetMinJingWangGeTree`,

  GetDailyCheckHistory: `${apiPreFix}DailyCheck/GetDailyCheckHistory`,


  GetKaoHeHistoryById: `${apiPreFix}Kaohe/GetKaoHeHistoryById`,


  UpdateFile: `${apiPreFix}FileUpdate/UpdateFile`,
  SearchFace: `${apiPreFix}FileUpdate/SearchFace`,

  // 公告管理
  GetNews: `${apiPreFix}`,
  SaveNews: `${apiPreFix}`,

  // 人员标记
  MarkChange: `${apiPreFix}user/SetMark`,

  // 房屋列表
  getFangWuList: `${apiPreFix}fangwu/GetAllFangWu`,

};

export const API = sAPI;
