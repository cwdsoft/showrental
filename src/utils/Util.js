import {
  isNull,
  isUndefined,
  isNumber,
  isString
} from 'lodash/lang'

export const actionsFactory = (actions, ns) => {
  let re = /_(\w)/g;
  Object.entries(actions).forEach(([key, value]) => {
    let fn = key.toLowerCase().replace(re, ($0, $1) => $1.toUpperCase());
    actions[fn] = actionCreatorFactory(`${ns}/${value}`);
  });

  return actions;
};

export const actionCreatorFactory = (type, ...argNames) => (...args) => {
  let action = {
    type
  };
  let defaultArg = 'payload';
  if (!argNames.length) {
    action[defaultArg] = args[0];
  } else {
    argNames.forEach((arg, index) => {
      action[arg] = args[index];
    });
  }
  return action;
};

export function isNoData(value) {
  if (isNull(value)) return true
  if (Array.isArray(value) && value.length <= 0) return true
  if (isUndefined(value)) return true
  if (isNumber(value)) return false
  if (isString(value) && (value.trim() === '')) return true
  return false
}
