import {
  PEOPLE,
  FACEANLY,
  PATHANLY,
  HOUSE,
  VIEW,
  STRUCT,
  TJ,
  CHECKLOG,
  LOGIN,
  PROBLEMROOM,
  PERFORMANCELOG,
  SYSTEMMSG,
  NEWS,
  NEWSMANA,
  THRIRECOLOR
} from './NameSpace';

const context = '';

export default {

  THRIRECOLOR: `${context}/${THRIRECOLOR}`,
  SYSTEMMSG: `${context}/${SYSTEMMSG}`,
  NEWS: `${context}/${NEWS}`,
  NEWSMANA: `${context}/${NEWSMANA}`,

  PROBLEMROOM: `${context}/${PROBLEMROOM}`,
  PERFORMANCELOG: `${context}/${PERFORMANCELOG}`,

  LOGIN: `${context}/${LOGIN}`,
  CHECKLOG: `${context}/${CHECKLOG}`,
  TJ: `${context}/${TJ}`,
  STRUCT: `${context}/${STRUCT}`,
  FACEANLY: `${context}/${FACEANLY}`,
  PATHANLY: `${context}/${PATHANLY}`,
  PEOPLE: `${context}/${PEOPLE}`,
  HOUSE: `${context}/${HOUSE}`,
  VIEW: `${context}/${VIEW}`,
  ROOT: '/'
}

