export const COMMON = 'COMMON';
export const PEOPLE = 'PEOPLE';
export const FACEANLY = 'FACEANLY';
export const PATHANLY = 'PATHANLY';
export const HOUSE = 'HOUSE';
export const VIEW = 'VIEW';
export const STRUCT = 'STRUCT';
export const TJ = 'TJ';
export const CHECKLOG = 'CHECKLOG';
export const PROBLEMROOM = 'PROBLEMROOM';
export const PERFORMANCELOG = 'PERFORMANCELOG';
export const NEWS = 'NEWS';
export const SYSTEMMSG = 'SYSTEMMSG';
export const NEWSMANA = 'NEWSMANA';
export const THRIRECOLOR = 'THRIRECOLOR';

export const LOGIN = 'LOGIN';


export const allName = [
  'COMMON',
  'PEOPLE',
  'FACEANLY',
  'PATHANLY',
  'HOUSE',
  'VIEW',
  'STRUCT',
  'TJ',
  'CHECKLOG',
  'PROBLEMROOM',
  'PERFORMANCELOG',
  'NEWS',
  'SYSTEMMSG',
  'NEWSMANA',
  'THRIRECOLOR'
]
