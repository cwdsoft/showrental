/* eslint-env browser */
// import { getCookieByName, setCookie, NotificationUtil } from './Util';
import queryString from 'query-string';
import { notification } from 'antd';


function Notification(type, options) {
  notification[type](Object.assign({}, options));
}

let AjaxInstance = new window.Ajax({ queryString, Notification });


const { request, PUT, GET, POST, DELETE } = AjaxInstance;

export { request, PUT, GET, POST, DELETE }
