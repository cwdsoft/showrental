import React, { Component } from 'react';
import { Statistic, Card, Row, Col, Icon, List } from 'antd';
import { connect } from 'dva';
import moment from 'moment'
import { withRouter, routerRedux } from 'dva/router';
import { VIEW, COMMON } from '../../utils/NameSpace';
// import actionHelper from '../../action/common'
import MapFenbu from './mapfenbu'
// import Personinfo from '../common/personinfo';
import RoutePath from '../../utils/RoutePath';
import { isNoData } from '../../utils/Util'

const gridStyle = {
  width: '16.666%',
  textAlign: 'center',
};
const extraStyle = {
  padding: '4px 10px',
};


class ViewMainPage extends Component {
  getRank = (oldRank) => {
    if (oldRank.length > 10) {
      return oldRank.slice(0, 10)
    }
    return oldRank
  }
  jump = (level) => {
    this.props.dispatch(routerRedux.push(RoutePath.PEOPLE + '?level=' + level));
  }
  newjump = (level, time) => {
    this.props.dispatch(routerRedux.push(RoutePath.PEOPLE + '?level=' + level + `&from=${time}&to=${time}`));
  }
  jumpToProblemRoom = (level) => {
    this.props.dispatch(routerRedux.push(RoutePath.PROBLEMROOM + '?level=' + level));
  }
  jumpToColorPeople = (level) => {
    this.props.dispatch(routerRedux.push(RoutePath.THRIRECOLOR + '?level=' + level));
  }
  jumpToJixiao = (userid) => {
    this.props.dispatch(routerRedux.push(RoutePath.PERFORMANCELOG + '?userid=' + userid));
  }
  jumpFangwu = () => {
    this.props.dispatch(routerRedux.push(RoutePath.HOUSE));
  }
  render() {
    const { baseInfo, jixiaoRank } = this.props;
    let {
      fangDongCount,
      fangDongNewCount,
      minJingCount,
      wangGeYuanCount,
      zuKeCount,
      zuKeNewCount,
      fangWuCount,
      star1Count,
      star2Count,
      star3Count,
      star4Count,
      star5Count,
      yelloPeopleCount,
      redPeopleCount,
    } = baseInfo
    let a5 = Array.from(Array(5), () => 1);
    let a4 = Array.from(Array(4), () => 1);
    let a3 = Array.from(Array(3), () => 1);
    let a2 = Array.from(Array(2), () => 1);
    let a1 = Array.from(Array(1), () => 1);
    return (
      <div>
        {/* <div style={{ background: '#ECECEC', padding: '30px' }}> */}
        <Row gutter={16}>
          <Col span={24}>
            <Card >
              <Card.Grid style={gridStyle} onClick={() => { this.jump(5) }} ><Statistic title="房东总数" valueStyle={{ cursor: 'pointer' }} value={fangDongCount} /></Card.Grid>
              <Card.Grid style={gridStyle} onClick={() => { this.jump(4) }} ><Statistic title="租客总数" valueStyle={{ cursor: 'pointer' }} value={zuKeCount} /></Card.Grid>
              <Card.Grid
                style={Object.assign({}, gridStyle, {})}
                onClick={() => { this.jumpToColorPeople('严管类') }}
              ><Statistic title="严管类人" valueStyle={{ cursor: 'pointer', color: '#a70000' }} value={redPeopleCount} /></Card.Grid>
              <Card.Grid
                style={Object.assign({},
                gridStyle, {

                })}
                onClick={() => { this.jumpToColorPeople('关注类') }}
              ><Statistic title="关注类人" valueStyle={{ cursor: 'pointer', color: '#c5af00' }} value={yelloPeopleCount} /></Card.Grid>
              <Card.Grid style={gridStyle} onClick={() => { this.jump(7) }} ><Statistic title="民警总数" valueStyle={{ cursor: 'pointer' }} value={minJingCount} /></Card.Grid>
              <Card.Grid style={gridStyle} onClick={() => { this.jump(6) }} ><Statistic title="网格员总数" valueStyle={{ cursor: 'pointer' }} value={wangGeYuanCount} /></Card.Grid>
            </Card>
          </Col>
        </Row>
        <div style={{ height: '24px' }} />
        <Row gutter={16}>
          <Col span={4}>
            <Card
              hoverable
              onClick={() => {
              this.newjump(5, moment().format('MM/DD/YYYY'))
            }}
            >
              <Statistic
                title="今日房东变化"
                value={fangDongNewCount}
                precision={0}
                valueStyle={{ color: 'red' }}
                prefix={<Icon type="arrow-up" />}
                suffix="人"
              />
            </Card>
            <Card
              hoverable
              onClick={() => {
               this.newjump(4, moment().format('MM/DD/YYYY'))
              }}
            >
              <Statistic
                title="今日房客变化"
                value={zuKeNewCount}
                precision={0}
                    //  valueStyle={{ color: '#cf1322' }}
                    //  prefix={<Icon type="arrow-down" />}
                valueStyle={{ color: 'red' }}
                prefix={<Icon type="arrow-up" />}
                suffix="人"
              />
            </Card>
            <div style={{ height: '24px' }} />
            <List
              size="small"
              header={<div>网格员当月绩效排行</div>}
              bordered
              dataSource={this.getRank(jixiaoRank)}
              renderItem={(item, index) => {
                if (isNoData(item.name)) {
                  return (<List.Item>{index + 1}. 空缺</List.Item>)
                }
                let icn = null;
                if (index === 0) {
                  icn = (<React.Fragment>
                    <Icon type="heart" theme="twoTone" twoToneColor="#eb2f96" />
                    <Icon type="heart" theme="twoTone" twoToneColor="#eb2f96" />
                    <Icon type="heart" theme="twoTone" twoToneColor="#eb2f96" />
                  </React.Fragment>)
                } else if (index === 1) {
                  icn = (<React.Fragment>
                    <Icon type="heart" theme="twoTone" twoToneColor="#eb2f96" />
                    <Icon type="heart" theme="twoTone" twoToneColor="#eb2f96" />
                  </React.Fragment>)
                } else if (index === 2) {
                  icn = <Icon type="heart" theme="twoTone" twoToneColor="#eb2f96" />
                }
               return (<List.Item >
                 {index + 1}. <a onClick={() => { this.jumpToJixiao(item.userid) }} >{item.name}</a> 共{item.value}分 <span style={{ textIndent: '4px' }} />{icn}</List.Item>)
              }}
            />
          </Col>
          <Col span={20}>
            <Row gutter={16} />
            <Col span={24}>
              <Card.Grid
                style={Object.assign({}, gridStyle, extraStyle)}
                onClick={() => { this.jumpFangwu() }}
              >
                <Statistic
                  title="房屋总数"
                  valueStyle={{ cursor: 'pointer', fontSize: '16px' }}
                  value={fangWuCount}
                />
                <div>
                  {
                    <img style={{ width: '20px', height: '20px' }} alt="star" src="./loc/image/house.png" />
                  }
                </div>
              </Card.Grid>
              <Card.Grid
                style={Object.assign({}, gridStyle, extraStyle)}
                onClick={() => { this.jumpToProblemRoom(1) }}
              >
                <Statistic
                  title="不合格房屋"
                  valueStyle={{ cursor: 'pointer', fontSize: '16px' }}
                  value={star1Count}
                />
                <div>
                  {
                    a1.map((ele, i) => <img key={i} style={{ width: '20px', height: '20px' }} alt="star" src="./loc/image/star.png" />)
                  }
                </div>
              </Card.Grid>
              <Card.Grid
                style={Object.assign({}, gridStyle, extraStyle)}
                onClick={() => { this.jumpToProblemRoom(2) }}
              >
                <Statistic
                  title="基本合格房屋"
                  valueStyle={{ cursor: 'pointer', fontSize: '16px' }}
                  value={star2Count}
                />

                <div>
                  {
                    a2.map((ele, i) => <img key={i} style={{ width: '20px', height: '20px' }} alt="star" src="./loc/image/star.png" />)
                  }
                </div></Card.Grid>
              <Card.Grid
                style={Object.assign({}, gridStyle, extraStyle)}
                onClick={() => { this.jumpToProblemRoom(3) }}
              >
                <Statistic
                  title="合格房屋"
                  valueStyle={{ cursor: 'pointer', fontSize: '16px' }}
                  value={star3Count}
                />
                <div>
                  {
                    a3.map((ele, i) => <img key={i} style={{ width: '20px', height: '20px' }} alt="star" src="./loc/image/star.png" />)
                  }
                </div>
              </Card.Grid>
              <Card.Grid
                style={Object.assign({}, gridStyle, extraStyle)}
                onClick={() => { this.jumpToProblemRoom(4) }}
              >
                <Statistic
                  title="良好房屋"
                  valueStyle={{ cursor: 'pointer', fontSize: '16px' }}
                  value={star4Count}
                />
                <div>
                  {
                    a4.map((ele, i) => <img key={i} style={{ width: '20px', height: '20px' }} alt="star" src="./loc/image/star.png" />)
                  }
                </div>
              </Card.Grid>
              <Card.Grid
                style={Object.assign({}, gridStyle, extraStyle)}
                onClick={() => { this.jumpToProblemRoom(5) }}
              >
                <Statistic
                  title="优秀房屋"
                  valueStyle={{ cursor: 'pointer', fontSize: '16px' }}
                  value={star5Count}
                />
                <div>
                  {
                    a5.map((ele, i) => <img key={i} style={{ width: '20px', height: '20px' }} alt="star" src="./loc/image/star.png" />)
                  }
                </div>
              </Card.Grid>
            </Col>
            <Col span={24}>
              <MapFenbu />

            </Col>
          </Col>
        </Row>
        {/* </div> */}
      </div>
    );
  }
}

export const mapStateToProps = (state) => {
  const { baseInfo, jixiaoRank } = state[VIEW];
  const { peopleInfo } = state[COMMON]
  return {
    baseInfo,
    peopleInfo,
    jixiaoRank
  }
}

export default withRouter(connect(mapStateToProps)(ViewMainPage));
