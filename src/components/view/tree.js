import React, { Component } from 'react';
import { connect } from 'dva';
import ReactEcharts from 'echarts-for-react';
import { VIEW } from '../../utils/NameSpace';

class TubiaoPage extends Component {
    getOption = () => {
      const option = {
        tooltip: {
          trigger: 'item',
          triggerOn: 'mousemove'
        },
        series: [
          {
            type: 'tree',

            data: this.props.tree,

            top: '1%',
            left: '7%',
            bottom: '1%',
            right: '20%',

            symbolSize: 7,

            label: {
              normal: {
                position: 'left',
                verticalAlign: 'middle',
                align: 'right',
                fontSize: 9
              }
            },

            leaves: {
              label: {
                normal: {
                  position: 'right',
                  verticalAlign: 'middle',
                  align: 'left'
                }
              }
            },

            expandAndCollapse: true,
            animationDuration: 550,
            animationDurationUpdate: 750
          }
        ]
      }

      return option
    }
    render() {
      // const {} = this.props;
      console.log('tree: ', this.props.tree)
      return (
        <div style={{ marginTop: '24px' }}>

          <ReactEcharts option={this.getOption()} />
        </div>
      );
    }
}

export const mapStateToProps = (state) => {
  const { tree } = state[VIEW];
  return {
    tree
  }
}

export default connect(mapStateToProps)(TubiaoPage);
