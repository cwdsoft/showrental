import React, { Component } from 'react';
import { connect } from 'dva';
import { Modal } from 'antd';
import { routerRedux } from 'dva/router';
// import BMap from 'BMap'
// import ReactEcharts from 'echarts-for-react';
import RoutePath from '../../utils/RoutePath';
import { VIEW } from '../../utils/NameSpace';
// import actionHelper from '../../action/common'

// function mark(arr, map) {
//   arr.forEach((pt) => {
//     let BMap = window.BMap // 取出window中的BMap对象
//     let point = new BMap.Point(pt.x, pt.y);
//     map.centerAndZoom(point, 15);
//     let marker = new BMap.Marker(point); // 创建标注
//     map.addOverlay(marker);
//   })
// }

class MapFenbu extends Component {
  state = {
    modalVisible: false,
    content: [{ label: '房东姓名', value: '小李' }, { label: '地址', value: '西安' }]
  }
  componentDidMount() {
    let BMap = window.BMap // 取出window中的BMap对象
    let map = new BMap.Map('allmap'); // 创建Map实例
    this.map = map;
    map.centerAndZoom(new BMap.Point(110.149280, 35.237520), 15); // 初始化地图,设置中心点坐标和地图级别
    map.addControl(new BMap.MapTypeControl()); // 添加地图类型控件
    map.setCurrentCity('渭南'); // 设置地图显示的城市 此项是必须设置的
    map.enableScrollWheelZoom(true); // 开启鼠标滚轮缩放d
  }
  componentWillReceiveProps(props) {
    let map = this.map
    let BMap = window.BMap // 取出window中的BMap对象
    const cp = props.housePosi.map((ele) => {
      return new BMap.Point(ele.y, ele.x)
    })
    setTimeout(() => {
      let convertor = new BMap.Convertor();
      for (let i = 0; i <= cp.length / 10; i++) {
        let v = []
        let chach = []
        if ((cp.length / 10).toFixed(0) === String(i)) {
          v = cp.slice(-(cp.length % 10))
          chach = props.housePosi.slice(-(cp.length % 10))
        } else {
          v = cp.slice(i * 10, (i + 1) * 10)
          chach = props.housePosi.slice(i * 10, (i + 1) * 10)
        }
        convertor.translate(v, 3, 5, (data) => {
          return this.translateCallback(data, map, chach)
        })
      }
    }, 1000)
  }
  onMarkerClick = (e) => {
    const info = e.target.info;
    this.setState({
      modalVisible: true,
      content: info
    })
  }
  setModalVisible = () => {
    this.setState({
      modalVisible: false
    })
  }
  jump = (fangwuid) => {
    this.props.dispatch(routerRedux.push(RoutePath.HOUSE + '?query=' + fangwuid));
  }
  translateCallback = (data, map, oldData) => {
    if (data.status === 0) {
      let BMap = window.BMap // 取出window中的BMap对象
      for (let i = 0; i < data.points.length; i++) {
        let marker = new BMap.Marker(data.points[i])
        marker.address = oldData[i].address
        marker.fangDongName = oldData[i].fangDongName
        marker.addEventListener('onclick', (e) => {
          if (e.target.address) {
            this.jump(e.target.address)
          }
          console.log('onclick ：', e.target)
        })
        let label = new BMap.Label(marker.address + ',' + marker.fangDongName, {
          offset: new BMap.Size(20, -10)
        });
        marker.setLabel(label)
        label.setStyle({
          display: 'none'
        });
        marker.addEventListener('onmouseover', (e) => {
          console.log('e ：', e)
          label.setStyle({
            display: 'block'
          });
        })
        marker.addEventListener('onmouseout', (e) => {
          console.log('e out：', e)
          label.setStyle({
            display: 'none'
          });
        })
        map.addOverlay(marker);
        map.setCenter(data.points[i]);
      }
    }
  }
  render() {
    // const {} = this.props;
    return (
      <div>
        <Modal
          title="房屋信息"
          style={{ top: 20 }}
          visible={this.state.modalVisible}
          onOk={this.setModalVisible}
          onCancel={this.setModalVisible}
          footer={null}
        >
          {
          this.state.content.map((ele, i) => <p key={i} >{ele.label}: {ele.value}</p>)
        }

        </Modal>
        <div>
          <span style={{ borderLeft: '2px solid green', paddingLeft: '10px' }} >房屋分布</span>
          <div
            id="allmap"
            style={{
              width: '100%',
              height: '100vh'
            }}
          />
        </div>
      </div>
    );
  }
}

export const mapStateToProps = (state) => {
  const { housePosi } = state[VIEW];
  console.log('housePosi', housePosi)
  return {
    housePosi
  }
}

export default connect(mapStateToProps)(MapFenbu);
