import React, { Component } from 'react';
import { connect } from 'dva';
import ReactEcharts from 'echarts-for-react';
import { COMMON } from '../../utils/NameSpace';
// import actionHelper from '../../action/common'

class TubiaoPage extends Component {
    state = {

    }
    getOption = () => {
      const option = {
        title: {
          text: '合阳县登记人口变化趋势图'
        },
        tooltip: {
          trigger: 'axis'
        },
        legend: {
          data: ['民警', '网格员', '房东', '租客']
        },
        grid: {
          left: '3%',
          right: '4%',
          bottom: '3%',
          containLabel: true
        },
        toolbox: {
          feature: {
            saveAsImage: {}
          }
        },
        xAxis: {
          type: 'category',
          boundaryGap: false,
          data: ['3/20', '3/24', '3/28', '4/3', '4/8', '4/11']
        },
        yAxis: {
          type: 'value'
        },
        series: [
          {
            name: '民警',
            type: 'line',
            stack: '总量',
            data: [0, 1, 2, 3, 4, 5]
          },
          {
            name: '网格员',
            type: 'line',
            stack: '总量',
            data: [0, 0, 0, 1, 2, 12]
          },
          {
            name: '房东',
            type: 'line',
            stack: '总量',
            data: [0, 10, 33, 42, 85, 103]
          },
          {
            name: '租客',
            type: 'line',
            stack: '总量',
            data: [0, 84, 124, 230, 264, 500]
          },
        ]
      };
      return option
    }
    render() {
      // const {} = this.props;
      return (
        <div style={{ marginTop: '24px' }}>
          <ReactEcharts option={this.getOption()} />
        </div>
      );
    }
}

export const mapStateToProps = (state) => {
  const { selectedKeys } = state[COMMON];
  return {
    selectedKeys
  }
}

export default connect(mapStateToProps)(TubiaoPage);
