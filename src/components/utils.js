
export const getExcelData = (columns, dataSource) => {
  let sheetFilter = columns.map(ele => ele.dataIndex);
  let sheetHeader = columns.map(ele => ele.title);
  let sheetName = 'sheet';
  let sheetData = dataSource;
  return [{
    sheetFilter,
    sheetHeader,
    sheetName,
    sheetData
  }]
};

