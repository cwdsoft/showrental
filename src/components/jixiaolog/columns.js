export const columns = [
  {
    title: '考核时间',
    dataIndex: 'kaoHeMonth',
    key: 'kaoHeMonth',
    visible: true,
    // renderFn: 'renderLink',
  },
  {
    title: '网格员姓名',
    dataIndex: 'peopleName',
    key: 'peopleName',
    visible: true,
    // renderFn: 'renderLink',
  },
  {
    title: '绩效分',
    dataIndex: 'score',
    key: 'score',
    visible: true,
    // renderFn: 'renderLink',
  },
  // {
  //   title: '绩效详情',
  //   dataIndex: 'kaoHeRecord',
  //   key: 'kaoHeRecord',
  //   visible: true,
  //   // renderFn: 'renderLink',
  // },
]

export const kaoHeRecordColumns = [
  {
    title: '提交时间',
    dataIndex: 'createTime',
    key: 'createTime',
    visible: true,
    // renderFn: 'renderLink',
  },
  {
    title: '绩效项',
    dataIndex: 'kaoHeItem',
    key: 'kaoHeItem',
    visible: true,
    // renderFn: 'renderLink',
  },
  {
    title: '详细描述',
    dataIndex: 'detail',
    key: 'detail',
    visible: true,
    // renderFn: 'renderLink',
  },
  {
    title: '审核时间',
    dataIndex: 'shenHeTime',
    key: 'shenHeTime',
    visible: true,
    // renderFn: 'renderLink',
  },
  // {
  //   title: '审核结果',
  //   dataIndex: 'result',
  //   key: 'result',
  //   visible: true,
  //   // renderFn: 'renderLink',
  // },
]
