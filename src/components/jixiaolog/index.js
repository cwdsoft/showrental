import React, { Component } from 'react'
import {
  Table, Row, Col
} from 'antd';
import { connect } from 'dva';
// import Highlighter from 'react-highlight-words';
import { COMMON } from '../../utils/NameSpace';
import { isNoData } from '../../utils/Util'
import TextEllipse from '../common/textEllipse';
import ImgLun from '../common/imgLun';
import { columns as constColumns, kaoHeRecordColumns } from './columns.js'
import SearchForm from './search';
// import ResultModal from '../problemRoom/resultModal'

class JixiaoTable extends Component {
  state = {
    // searchText: '',
  };
  getColumns = (columns) => {
    const TableColumns = columns.filter(ele => ele.visible).map((item) => {
      // if (!isNoData(item.sortFn)) { this[item.sortFn](item, item.dataIndex) }
      if (item.filterable) { this.filterColumn(item, item.dataIndex) }
      if (isNoData(item.renderFn)) { this.renderColumn(item); return item; }
      this[item.renderFn](item, item.dataIndex);
      return item;
    });
    return TableColumns;
  }

  jilianChange = (value) => {
    const { roomTree } = this.props;
    console.log(value, roomTree);
  }
  filterColumn = (column, dataIndex) => {
    const { dataSource } = this.props;
    let Box = new Set();
    dataSource.forEach((element) => {
      if (isNoData(element[dataIndex])) return;
      Box.add(element[dataIndex])
    });
    column.filters = Array.from(Box, (ele) => { return { text: ele, value: ele } })
    column.onFilter = (value, record) => {
      if (isNoData(record[dataIndex])) return false;
      return record[dataIndex].toString() === value.toString()
    }
  }
  doOtherRow = (record) => {
    return (
      <div>
        <Table
          columns={this.getColumns(kaoHeRecordColumns)}
          dataSource={record.kaoHeRecord}
          pagination={false}
        />
      </div>
    );
  };
  renderColumn = (column) => {
    column.render = (text) => {
      return (
        <TextEllipse >{text}</TextEllipse>
      );
    }
  }
  renderimage = (column) => {
    column.render = () => {
      const imgBox = [
        'https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png',

        'https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png'
      ]
      return (
        <ImgLun imgBox={imgBox}>
          <img style={{ width: '50px', height: '50px' }} alt="example" src={imgBox[0]} />
        </ImgLun>

      );
    }
  }
  // renderResult = (column) => {
  //   column.render = (text, record) => {
  //     console.log(record, 'problem')
  //     return (
  //       <ResultModal {...record} >查看</ResultModal>
  //     );
  //   }
  // }

  render() {
    const { dataSource, jixiaoexpendData } = this.props;
    return (
      <div>
        <Row>
          <Col span={24} >
            <SearchForm />
          </Col>
        </Row>
        <div style={{ height: '12px' }} />
        <Table
          columns={this.getColumns(constColumns)}
          dataSource={dataSource}
          expandedRowRender={record => this.doOtherRow(record, jixiaoexpendData)}
          pagination={false}
          size="small"
        />
      </div>);
  }
}

export const mapStateToProps = (state) => {
  const { jixiaoLogSource = [], jixiaoexpendData } = state[COMMON];
  return {
    dataSource: jixiaoLogSource, jixiaoexpendData
  }
}

export default connect(mapStateToProps)(JixiaoTable);
