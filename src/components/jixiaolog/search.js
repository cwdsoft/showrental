import React, { Component } from 'react';
import { Form, Button, Select } from 'antd';
import { connect } from 'dva';
import { isNoData } from '../../utils/Util';
import { COMMON } from '../../utils/NameSpace';
import actionHelper from '../../action/common'

const { Option, OptGroup } = Select;
const formItemLayout = 'vertical';

class RichTextPage extends Component {
  getWangeOption = () => {
    return this.props.mjWgyTree.map((ele, i) => {
      return (<OptGroup key={i} label={ele.label}>
        {
         ele.children.map((ppp, ii) =>
           <Option key={ppp + ii} value={ppp.value}>{ppp.label}</Option>
            )
        }
      </OptGroup>)
    })
  }
  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
        let [HandlerId] = [-1];
        if (!isNoData(values.HandlerId)) {
          HandlerId = values.HandlerId
        }
        let fin = {
          id: HandlerId
        }
        console.log('Received values of form: ', fin);

        this.props.dispatch(actionHelper.getKaoheLog(fin)).then((result) => {
          if (result) {
            // notification.success({
            //   message: '恭喜你',
            //   description: '发布成功!',
            // });
          }
        })
      }
    });
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <div>
        <Form layout="inline" onSubmit={this.handleSubmit} >
          <Form.Item
            label="网格员"
            {...formItemLayout}
          >
            {getFieldDecorator('HandlerId')(
              <Select
                allowClear
                style={{ width: 200 }}
              >
                {
                  this.getWangeOption()
                }
              </Select>
          )}
          </Form.Item>
          <Form.Item>
            <Button type="primary" htmlType="submit">查询</Button>
          </Form.Item>
        </Form>
      </div>
    );
  }
}

export const mapStateToProps = (state
) => {
  const { mjWgyTree } = state[COMMON];
  return {
    mjWgyTree
  }
}

const RichTextPageWrapp = Form.create()(RichTextPage);


export default connect(mapStateToProps)(RichTextPageWrapp);
