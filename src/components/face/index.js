import React, { Component } from 'react';
import { Row, Col, Spin, Badge, Modal, Card, Skeleton } from 'antd';
import { connect } from 'dva';
import { COMMON } from '../../utils/NameSpace';
// import actionHelper from '../../action/common'
import css from './index.css'
import DragUpload from './upload'

// const { Meta } = Card;


class FaceMainPage extends Component {
  state = {
    userInfo: {
      name: '',
      address: '',
      idCard: '',
      phoneNumber: '',
      work: '',
      createTime: '',
      pic: ''
    },
    loading: false,
    visible: false
  }
  showModal = (userInfo) => {
    // 设置userinfo到state
    console.log(userInfo)
    this.setState({
      visible: true,
      userInfo: userInfo
    });
  }

  handleOk = (e) => {
    console.log(e);
    this.setState({
      visible: false,
    });
  }

  handleCancel = (e) => {
    console.log(e);
    this.setState({
      visible: false,
    });
  }
  render() {
    const { searchData } = this.props;
    console.log('searchData: ', searchData)
    const { userInfo, visible } = this.state
    return (
      <div style={{ height: '100%' }} >
        <Spin spinning={this.state.loading}>
          {/* <Dragger {...props}>
            <p>点击或拖拽一张照片到此区域以上传图片</p>
          </Dragger>
          <div style={{ height: '8px' }} /> */}
          <Row gutter={24} type="flex" justify="space-around" >
            <Col span={10}>
              <div>
                <fieldset>
                  <legend>原始照片</legend>
                  <div style={{ border: '1px dotted #CCC', minHeight: '300px', width: '100%' }} >
                    <DragUpload />
                  </div>
                </fieldset>
              </div>
            </Col>
            <Col span={12}>
              <div>
                <fieldset>
                  <legend>对比结果</legend>
                  <div className={css.flowflex} >
                    {
                      searchData.length === 0 ?
                        <div style={{ border: '1px dotted #CCC', width: '100%', textAlign: 'center', lineHeight: '332px', color: '#CCC' }}>等待比对</div>
                        :
                        searchData.map((ele, i) => (
                          <div
                            key={i}
                            style={{ width: '105px', height: '105px', padding: '5px', cursor: 'pointer' }}
                            onClick={() => { this.showModal(ele) }}
                          >
                            <Badge
                              count={Number(ele.score).toFixed(2) + '%'}
                              style={{ backgroundColor: ele.score > 70 ? '#52c41a' : '#999' }}
                            >
                              <img
                                alt="比对结果"
                                style={{ width: '100px', height: '100px' }}
                                src={ele.picPath}
                              />
                            </Badge>
                          </div>))
                    }

                  </div>
                </fieldset>
              </div>
            </Col>
          </Row>
          <Modal
            title="详细信息"
            visible={visible}
            onOk={this.handleOk}
            onCancel={this.handleCancel}
            footer={null}
            width="400px"
          >
            <div>
              <Card
                hoverable
                style={{ marginBottom: '12px' }}
              >
                <Skeleton loading={false} avatar active>
                  {/* <Meta
                    avatar={<Avatar shape="square" size="large" src={userInfo.picPath} />}
                    title={userInfo.name}
                  /> */}
                  <img
                    style={{ width: '100%', minHeight: '80px' }}
                    alt="头像未录入"
                    src={userInfo.picPath || 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEASABIAAD/2wBDAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0aHBwgJC4nICIsIxwcKDcpLDAxNDQ0Hyc5PTgyPC4zNDL/wAALCAH0AfQBAREA/8QAGgABAQEBAQEBAAAAAAAAAAAAAAQBAgMFB//EACwQAQACAgEDBAEEAgIDAAAAAAABAgMRBCEycRIxQVEiE0JhkRShNFIjM7H/2gAIAQEAAD8A/aQAAAAAAAAAAAAAAAAAAABXTsr4SAAAAAAAAAAAAAAAAAAAACunZXwkAAAAAAAAAAAAAAAAAAAAFdOyvhIAAAAAAAAAAAAAAAAAAAAK6dlfCQAAAAAAAAAAAAAAAAAAAAV07K+EgAAAAAAAAAAAAAAAAAAAArp2V8JAAAAAAAAAAAAAAAAAAAABXTsr4SAAAAAAAAAAAAAAAAAAAACunZXwkAAAAAAAAAAAAAAAAAAAAFdOyvhIAAAAAAAAAbj7ZuD1M9X8N9R6o+27AAAAAAAAAV07K+EgAAAAAAADJnTPVudREvWvGzXjcTGnrXhf9v8A69Y4WGP2z/br/ExfU/2f4uL6/wBs/wAPD9T/AG5twcWvxj/bxtwrx26eN8eTH3f6cxff23bQAAAAAAAV07K+EgAAAAAAAyZ0Vi151SNyoxcLfXJuJV0xVpGo6vTQAAyYifh5X41L++48PG3Ap8Wtt5W4d47YmXE8fPH7P9uZx5Y96s1ePhn5fTesTqWgAAAAArp2V8JAAAAAAAGTL0w8e2WdzusePdfjw0xx0rG/vT0AAAABk1rPvEOf0qf9Y/onFjj9kf0+blvF8sTEaiPpgAAAAAK6dlfCQAAAAAAHMzpTx+N6p9V+q6IisREezQAAAAAT8rN+nTpPXb59YdAAAAAArp2V8JAAAAAABkzpTxuPMz67LojUAAAAAAPHkZ64a9fefZ861py39dvloAAAAACunZXwkAAAAAAZM6h68bDOW/qnpX4fRiIiOjQAAAAAHlnxRkp/Mez5sxOO3pt0mGgAAAAAK6dlfCQAAAAABlYm+SKx7TPV9TFjjHSK/TsAAAAAAEfMw7j11jrtJE7aAAAAACunZXwkAAAAABlp1Cvh4tbtb56wsAAAAAAAZMbjT5eXH+lliv31YAAAAACunZXwkAAAAABkV/UyRT7fUx19OOsfUOwAAAAAABJzce6Tf6hHWejQAAAABXTsr4SAAAAAA9eJT1ZYv9S+iAAAAAAADjLT145r9vl69OS9fqdNAAAAAFdOyvhIAAAAAMtOoWcGusVvKsAAAAAAAB8vPX05pn7lyAAAAAK6dlfCQAAAAAc39o8vp8evpo9QAAAAAAAEHPjV8cw8AAAAABXTsr4SAAAAADm/tHl9esarHhoAAAAAAACLnx1olj2aAAAAArp2V8JAAAAABxf48vsV7Y8NAAAAAAAARc79iWPZoAAAACunZXwkAAAAAHF/jy+xXthoAAAAAAACLnfsSx7NAAAAAV07K+EgAAAAA4v8eX2K9seGgAAAAAAAIud+xLHs0AAAABXTsr4SAAAAADi/x5fYr2x4aAAAAAAAAi537EsezQAAAAFdOyvhIAAAAAyN2nURL0jh5L6n1RD6UdIhoAAAAAAACflYLZoj0zrSO3HyY46zvw4ifuNNAAAABXTsr4SAAAAAzU2tFY95fRwYYx0jp111ewAAAAAAAABraHl4YrH6kJoaAAAAK6dlfCQAAAAHWCN8inl9QAAAAAAAAAHlyI3imHza+8tAAAAFdOyvhIAAAADcM65NPL6oAAAAAAAAAPHkz6cUy+dX3loAAAArp2V8JAAAAAZv03i30+nht6sVZ+4dgAAAAAAAACTm5Pw9Hykj2aAAAAK6dlfCQAAAAGTG4U8PL1ms+0ey0AAAAAAAABlp1Ey+Zlv+rl9XwwAAAAFdOyvhIAAAAAyJml4tH2+lhyRkpFnoAAAAAAAACTl5vTHprPXfVJWNQ0AAAAFdOyvhIAAAAAyfZ6cfLOK+p7fp9GJiY6NAAAAAAAAeebJGOkzvrro+bMzkvN5+WgAAAAK6dlfCQAAAAAZMbU8XkdfRaesrQAAAAAAAc3vFKzafZ83NlnNk9+kT0Z7AAAAACunZXwkAAAAABzMddwt43IiY9Fukx0hUAAAAAAAy1orG5fOz55y21Ha84jUNAAAAAFdOyvhIAAAAAA561ncTrT6PGyevFWZnq9gAAAAAAQ8vLM2mlZ1pPEaaAAAAACunZXwkAAAAAAJ9npxcnoyTEz00+iAAAAAAOMlvRSZ/h8ybfqX9c/LQAAAAAFdOyvhIAAAAAAObR1iX0ePl/Vpt7AAAAAAIeXl9UxSvxPVPEahoAAAAACunZXwkAAAAAAB1gyfpZOva+lWdxEtAAAAAHlnyxjxz16/D53W1ptPy0AAAAAAV07K+EgAAAAAAMtG4U8XP+y0+NrQAAAAGWtFY3M6fMzZZy3/iGAAAAAAArp2V8JAAAAAAAHM7ifVX3hfxs8Xr6Z9491AAAAATOofP5Oeck+iva8YjTQAAAAAAV07K+EgAAAAAAA561mLR8dX0uPknLhi0+8vUAAABDy80+qaR8J4jTQAAAAAAFdOyvhIAAAAAAAMt2ys4U/8AiiFQAAAD5nJ/5NnIAAAAAAArp2V8JAAAAAAABlu2VfC/9cKwAAAHzOR/ybOQAAAAAABXTsr4SAAAAAAADLe0ruHGuPVQAAAA+dyo1mmXmAAAAAAAK6dlfCQAAAAAAAc9bXrEfb6mKnoxxV2AAAAk5uObY4mI67SVnbQAAAAAAFdOyvhIAAAAAAA5mfiOsq+Nx9fnbrM9YifhYAAAAMtWLRqXzc2GcV+nWriJaAAAAAACunZXwkAAAAAABzM7nULOPxfTq14iZ+FfsAAAAA5vSt66tG4fPzYLYp3GvS84nbQAAAAABXTsr4SAAAAAAMmdNpjvln8Y3C7Dxq4uvXf8vcAAAAAGTETGpR5+J1m1NzKXc1n026S6AAAAAAV07K+EgAAAAAzcEeq/SsTPhRh4kzq15nxMLK0rSOkRHh0AAAAAADyyYK5I9oifvSLJxr4p/HdoeXq10npLrYAAAAArp2V8JAAAAAZ6iItadRWVGPh2nraYmFdMNMfbXTsAAAAAAACY28MnFpfcxWN/aTJxr4+u9x/Dy3Me9ZbEw0AAAAV07K+EgAAAEzpzvc6h7U4uW/WdaUU4eOvWYnaitYrGoaAAAAAAAAAPO+GuTuhPk4Ufsjr/ACnvhyY+7WnEWh0AAACunZXwkAAAZMxBWL5J1SNvfHw5t1vuFdMVaRqI29AAAAAAAAAAAGTET7w8cvFpk+deEl+Nkp2xMw8vVqdT0l0AAArp2V8JAAAc7mZ1WJnwoxcSbflaZ8TCymKlPasQ7AAAAAAAAAAAACY37vHJxqXjpERP3pFkwXxT03aPDiLNAAFdOyvhIAAyZ06xYr5p6e0fa7Fx6Y43r8vl7AAAAAAAAAAAAAAyY3GpS5uJE9ccREpJiaTqzdgAK6dlfCQAHMz11CnBxJn8sm4lbFYrERHw0AAAAAAAAAAAAAAHnlw1yxqf9IMuC+Kfb8XETEtAFdOyvhIAMiJvOqxtbg40Ujdus/zCkAAAAAAAAAAAAAAAGWrFo1MRKHPxprM3puf4h4RbbQFdOyvhIAyInJaK1+flfhwVx1jpG3ttuzZtmzZtuzZs2zbdmzZs2zbdm2bbtm27Zs23Zs2zZs23Zs2bZtuzZs2bZtuzbNt2zbds2T1Rcjja/Oka/iE0T8T7ugV07K+EgOdTe3pr7voYMMY6+3V7AAAAAAAAAAAAAAAAACLk8fX508ynrO4aK6dlfCQczPWI+13GweiNz7qAAAAAAAAAAAAAAAAAAZMbjSDk4Jpb11+ZeUTuGq6dlfCQenGrFsk7jb6AAAAAAAAAAAAAAAAAAA5yViaTuN9HzNas1ZTsr4f/2Q=='}
                  />
                  <p />
                  <p style={{ fontSize: '24px' }} >{userInfo.name}</p>
                  <p>手机号: {userInfo.phoneNumber}</p>
                  <p>身份证号: {userInfo.idcard}</p>
                  <p>地址: {userInfo.address}</p>
                  <p>工作: {userInfo.work}</p>
                  <p>创建时间: {userInfo.createTime}</p>
                </Skeleton>
              </Card>
            </div>
          </Modal>
        </Spin>

      </div>
    );
  }
}

export const mapStateToProps = (state) => {
  const { searchData, originPic } = state[COMMON];
  return {
    searchData, originPic
  }
}

export default connect(mapStateToProps)(FaceMainPage);
