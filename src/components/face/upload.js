import React, { Component } from 'react';
import { Form, Upload, message, Spin } from 'antd';
import { connect } from 'dva';
import './index.css';
import { isNoData } from '../../utils/Util';
import { COMMON } from '../../utils/NameSpace';
import actionHelper from '../../action/common'
import { API } from '../../utils/Constant'

// const formItemLayout = null;
const Dragger = Upload.Dragger;

class RichTextPage extends Component {
  state = {
    loading: false,
  }
  render() {
    const that = this
    const props = {
      showUploadList: false,
      name: 'file',
      multiple: false,
      action: API.SearchFace,
      headers: {
        Authorization: window.sessionStorage.getItem('openid')
      },
      onChange(info) {
        const status = info.file.status;
        that.setState({
          loading: true,
        });
        if (status !== 'uploading' && info.file.response) {
          if (info.file.response.status === 0) {
            if (isNoData(info.file.response.data)) {
              message.warning(' 未在库中检测到有效人脸！')
            }
            that.props.dispatch(actionHelper.setData({
              searchData: info.file.response.data || [],
              originPic: !isNoData(info.file.response.data) ? info.file.response.data[0].srcpath : ''
            }))
            that.setState({
              loading: false,
            });
          } else {
            that.props.dispatch(actionHelper.setData({
              searchData: [],
              originPic: !isNoData(info.file.response.data) ? info.file.response.data[0].srcpath : ''
            }))
            that.setState({
              loading: false,
            });
            message.warning(info.file.response.errorMsg + ' 未在库中检测到有效人脸！')
          }

        }
      },
    };

    return (
      <div style={{ width: '100%' }} >
        <Spin spinning={this.state.loading}>
          {
            <Dragger {...props} >
              {
                this.props.originPic ?
                  <img
                    alt="原始图片"
                    style={{ width: '100%' }}
                    src={this.props.originPic}
                  />
                :
                  <p style={{ height: '300px', lineHeight: '300px' }}>点击或拖拽一张照片到此区域以上传图片</p>
              }
            </Dragger>
          }
        </Spin>
      </div>
    );
  }
}

export const mapStateToProps = (
  state
) => {
  const { originPic } = state[COMMON];
  return {
    originPic
  }
}

const RichTextPageWrapp = Form.create()(RichTextPage);


export default connect(mapStateToProps)(RichTextPageWrapp);
