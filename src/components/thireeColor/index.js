import React, { Component } from 'react'
import ExportJsonExcel from 'js-export-excel'
import {
  Table, Select, BackTop, Spin, Row, Col, Button
  // Badge
} from 'antd';
import moment from 'moment'
import { connect } from 'dva';
import Highlighter from 'react-highlight-words';
import { COMMON } from '../../utils/NameSpace';
import { isNoData } from '../../utils/Util'
import TextEllipse from '../common/textEllipse';
import ImgLun from '../common/imgLun';
import { chengzurenColumns as constColumns } from './columns.js'
import actionhelper from '../../action/common';
import { getExcelData } from '../utils'

const Option = Select.Option;
const dateFormat = 'MM/DD/YYYY'

class HouseTable extends Component {
  state = {
    searchText: '',
  };

  getColumns = (columns) => {
    const TableColumns = columns.filter(ele => ele.visible).map((item) => {
      if (!isNoData(item.sortFn)) { this[item.sortFn](item, item.dataIndex) }
      if (item.filterable) { this.filterColumn(item, item.dataIndex) }
      if (isNoData(item.renderFn)) { this.renderColumn(item); return item; }
      this[item.renderFn](item, item.dataIndex);
      return item;
    });
    return TableColumns;
  }
  markChange = (mark, id) => {
    // this.props.dispatch(actionhelper.setData({ dataSource: [] }))
    const { originDataSource, dataSource } = this.props;
    // 需要更新本地
    this.props.dispatch(actionhelper.setData({
      originDataSource: originDataSource.map((ele) => {
        if (String(ele.id) === String(id)) {
          ele.mark = mark
        }
        return ele
      }),
      dataSource: dataSource.map((ele) => {
        if (String(ele.id) === String(id)) {
          ele.mark = mark
        }
        return ele
      }),
    }))
    this.props.dispatch(actionhelper.markChange({ mark, userId: id }))
  }
  handleChange = (value) => {
    this.props.dispatch(actionhelper.setData({
      currentPeopleClass: value,
      ColorPeopleDataSource:
      this.props.originDataSource.filter(ele =>
        String(ele.mark) === String(value)),
    }))
  }
  excelDownload = () => {
    const { dataSource } = this.props;
    let columns = this.getColumns(constColumns)
    let ds = this.filterData(dataSource)

    let option = {};
    option.fileName = '人员表'
    option.datas = getExcelData(columns, ds)
    // option.datas = [
    //   { sheetData: [{ one: '一行一列', two: '一行二列' }, { one: '二行一列', two: '二行二列' }],
    //     sheetName: 'sheet',
    //     sheetFilter: ['two', 'one'],
    //     sheetHeader: ['第一列', '第二列']
    //   },
    //   {
    //     sheetData: [{ one: '一行一列', two: '一行二列' }, { one: '二行一列', two: '二行二列' }]
    //   }];
    let toExcel = new ExportJsonExcel(option);
    toExcel.saveExcel();
  }
  filterData = (dataSource) => {
    const { startValue, endValue } = this.props
    let from = !isNoData(startValue) ? moment(startValue) : moment('01/01/1000')
    let to = !isNoData(endValue) ? moment(endValue) : moment('01/01/9999')
    return dataSource.filter((ele) => {
      let ct = moment(ele.createTime).format(dateFormat);
      let ft = moment(from).format(dateFormat);
      let tt = moment(to).format(dateFormat);
      if (moment(ct).isBefore(moment(ft))) {
        return false;
      }
      if (moment(tt).isSame(moment(ct))) {
        return true
      }
      if (moment(tt).isBefore(moment(ct))) {
        return false
      }
      return true
    })
  }
  filterColumn = (column, dataIndex) => {
    const { dataSource } = this.props;
    let Box = new Set();
    dataSource.forEach((element) => {
      if (isNoData(element[dataIndex])) return;
      Box.add(element[dataIndex])
    });
    column.filters = Array.from(Box, (ele) => { return { text: ele, value: ele } })
    column.onFilter = (value, record) => {
      if (isNoData(record[dataIndex])) return false;
      return record[dataIndex].toString() === value.toString()
    }
  }
  sortDate = (column, dataIndex) => {
    column.sorter = (a, b) => {
      if (isNoData(a[dataIndex]) && isNoData(b[dataIndex])) return 0
      if (isNoData(a[dataIndex])) return 1
      if (isNoData(b[dataIndex])) return -1
      const from = a[dataIndex];
      const to = b[dataIndex];
      if (moment(from).isSame(to)) {
        return 0;
      }
      if (moment(from).isBefore(to)) {
        return -1
      }
      return 1
    }
  }
  handleSearch = (selectedKeys, confirm) => {
    confirm();
    this.setState({ searchText: selectedKeys[0] });
  }

  handleReset = (clearFilters) => {
    clearFilters();
    this.setState({ searchText: '' });
  }
  // handleChange = (value) => {
  //   this.props.dispatch(actionhelper.setData({ currentLevel: value }))
  //   this.props.dispatch(actionhelper.getInit({ level: value }))
  // }
  searchAll = (value) => {
    const { originDataSource, dispatch } = this.props;
    if (isNoData(value)) {
      dispatch(actionhelper.setData({ dataSource: originDataSource }))
    }
    let fiDataSource = originDataSource.filter((ele) => {
      let lock = false;
      if (String(ele.name).includes(value)) {
        lock = true;
      }
      if (String(ele.idCard).includes(value)) {
        lock = true;
      }
      if (String(ele.phoneNumber).includes(value)) {
        lock = true;
      }
      if (String(ele.work).includes(value)) {
        lock = true;
      }
      if (String(ele.address).includes(value)) {
        lock = true;
      }
      if (String(ele.createTime).includes(value)) {
        lock = true;
      }
      return lock
    })
    this.setState({ searchText: value })
    dispatch(actionhelper.setData({ dataSource: fiDataSource }))
  }
  renderColumn = (column) => {
    column.render = (text) => {
      if (!text) {
        return text
      }
      return (
        <TextEllipse><Highlighter
          highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
          searchWords={[this.state.searchText]}
          autoEscape
          textToHighlight={text.toString()}
        /></TextEllipse>
      );
    }
  }


  renderDate = (column) => {
    column.render = (text) => {
      if (!text) {
        return text
      }
      return (
        <TextEllipse><Highlighter
          highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
          searchWords={[this.state.searchText]}
          autoEscape
          textToHighlight={moment(text).format(dateFormat).toString()}
        /></TextEllipse>
      );
    }
  }
  renderWork = (column) => {
    column.render = (text) => {
      if (!text) {
        return text
      }
      return (
        <TextEllipse><Highlighter
          highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
          searchWords={[this.state.searchText]}
          autoEscape
          textToHighlight={text.toString()}
        /></TextEllipse>
      );
    }
  }
  // renderLevel = (column) => {
  //   column.render = (text) => {
  //     if (!text) {
  //       return '放心人员'
  //     }
  //     let ss = '放心人员';
  //     switch (text) {
  //       case 'yellow': ss = '关注'; break;
  //       case 'red': ss = '严管'; break;
  //       default: ss = '放心'; break;
  //     }
  //     return (
  //       <TextEllipse>{ss}</TextEllipse>
  //     );
  //   }
  // }

  renderAction = (column) => {
    column.render = (text, record) => {
      return (<Select
        size="small"
        // defaultValue="green"
        value={record.mark || '放心类'}
        onChange={
        (value) => { this.markChange(value, record.id) }
        }
      >
        <Option value="严管类">严管类</Option>
        <Option value="关注类">关注类</Option>
        <Option value="放心类">放心类</Option>
      </Select>)

    }
  }

  renderimage = (column) => {
    column.render = (text, record) => {
      let head = []
      if (record.headpath) {
        head = [record.headpath]
      }
      return (
        <ImgLun imgBox={head}>
          {
            record.headpath ?
              <img style={{ width: '50px', height: '50px' }} alt="example" src={record.headpath} />
            : <div style={{ width: '50px', height: '50px' }} >213</div>
          }
        </ImgLun>

      );
    }
  }

  render() {
    const { dataSource, currentPeopleClass } = this.props;
    return (
      <div>
        <BackTop />
        <Row type="flex" justify="space-between" >
          <Col span={4} >
            <Select
              showSearch
              style={{ minWidth: '120px' }}
              placeholder="选择流动人口类别"
              onChange={this.handleChange}
              value={String(currentPeopleClass)}
            >
              <Option value="严管类">严管类</Option>
              <Option value="关注类">关注类</Option>
              <Option value="放心类">放心类</Option>
            </Select>
          </Col>
          <Col span={2} >
            <Button style={{ float: 'right' }} onClick={this.excelDownload} >Excel</Button>
          </Col>
        </Row>
        <div style={{ height: '12px' }} />
        <Spin spinning={this.props.loading}>
          <Table
            columns={this.getColumns(constColumns)}
            dataSource={dataSource}
            pagination={{
              size: 'small',
              pageSize: 20,
              pageSizeOptions: ['10', '20', '50', '100', '200'],
              showSizeChanger: true,
              showQuickJumper: true
            }}
            size="small"
            rowKey="id"
          />
        </Spin>
      </div>);
  }
}

export const mapStateToProps = (state) => {
  const {
    ColorPeopleDataSource,
    originColorPeopleDataSource,
    peopleInfo,
    currentPeopleClass
  } = state[COMMON];
  const effects = state.loading.effects
  return {
    originDataSource: originColorPeopleDataSource,
    dataSource: ColorPeopleDataSource,
    peopleInfo,
    currentPeopleClass,
    loading: !!effects['COMMON/getColorPeople'],
  }
}

export default connect(mapStateToProps)(HouseTable);
