
export const chengzurenColumns = [
  {
    title: '头像',
    dataIndex: 'avtor',
    key: 'avtor',
    visible: true,
    renderFn: 'renderimage',
    width: 100
  },
  {
    title: '姓名',
    dataIndex: 'name',
    key: 'name',
    visible: true,
  },
  {
    title: '身份证号码',
    dataIndex: 'idCard',
    key: 'idCard',
    visible: true,
  },
  {
    title: '手机号',
    dataIndex: 'phoneNumber',
    key: 'phoneNumber',
    visible: true,
  },
  {
    title: '工作',
    dataIndex: 'work',
    key: 'work',
    visible: true,
    filterable: true,
    renderFn: 'renderWork',
  },
  {
    title: '现住址',
    dataIndex: 'nowAddress',
    key: 'nowAddress',
    visible: true,
    width: 100
  },
  {
    title: '户籍住址',
    dataIndex: 'address',
    key: 'address',
    visible: true,
    width: 100
  },
  {
    title: '录入时间',
    dataIndex: 'createTime',
    key: 'createTime',
    visible: true,
    sortFn: 'sortDate',
    renderFn: 'renderDate',
  },
  {
    title: '人员分类',
    dataIndex: 'mark',
    key: 'mark',
    visible: true,
    filterable: true,
    // renderFn: 'renderLevel',
  },
]

