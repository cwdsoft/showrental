import React, { Component } from 'react';
import moment from 'moment'
import { Modal, Card, Row, Col, Skeleton } from 'antd';


class UserInfoModal extends Component {
    state = { visible: false }

  showModal = () => {
    if (this.props.clickBefore) {
      this.props.clickBefore()
    }
    this.setState({
      visible: true,
    });
  }

  handleOk = (e) => {
    console.log(e);
    this.setState({
      visible: false,
    });
  }

  handleCancel = (e) => {
    console.log(e);
    this.setState({
      visible: false,
    });
  }
  render() {
    const { userInfo = [], children } = this.props;
    const isOne = userInfo.length <= 1
    return (
      <div>
        <div>
          <a onClick={this.showModal} style={{ textDecoration: 'underline' }} >{children}</a>
          <Modal
            title="详细信息"
            visible={this.state.visible}
            onOk={this.handleOk}
            onCancel={this.handleCancel}
            footer={null}
            width={isOne ? '400px' : '1200px'}
          >
            <Row gutter={24} >
              {
              userInfo.map((ele, i) => {
                return (<Col key={i} span={isOne ? 24 : 8}><Card
                  hoverable
                  style={{ marginBottom: '12px' }}
                >
                  <Skeleton loading={false} avatar active>
                    <img
                      style={{ width: '100%', minHeight: '80px' }}
                      alt="头像未录入"
                      src={ele.headpath || 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEASABIAAD/2wBDAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0aHBwgJC4nICIsIxwcKDcpLDAxNDQ0Hyc5PTgyPC4zNDL/wAALCAH0AfQBAREA/8QAGgABAQEBAQEBAAAAAAAAAAAAAAQBAgMFB//EACwQAQACAgEDBAEEAgIDAAAAAAABAgMRBCEycRIxQVEiE0JhkRShNFIjM7H/2gAIAQEAAD8A/aQAAAAAAAAAAAAAAAAAAABXTsr4SAAAAAAAAAAAAAAAAAAAACunZXwkAAAAAAAAAAAAAAAAAAAAFdOyvhIAAAAAAAAAAAAAAAAAAAAK6dlfCQAAAAAAAAAAAAAAAAAAAAV07K+EgAAAAAAAAAAAAAAAAAAAArp2V8JAAAAAAAAAAAAAAAAAAAABXTsr4SAAAAAAAAAAAAAAAAAAAACunZXwkAAAAAAAAAAAAAAAAAAAAFdOyvhIAAAAAAAAAbj7ZuD1M9X8N9R6o+27AAAAAAAAAV07K+EgAAAAAAADJnTPVudREvWvGzXjcTGnrXhf9v8A69Y4WGP2z/br/ExfU/2f4uL6/wBs/wAPD9T/AG5twcWvxj/bxtwrx26eN8eTH3f6cxff23bQAAAAAAAV07K+EgAAAAAAAyZ0Vi151SNyoxcLfXJuJV0xVpGo6vTQAAyYifh5X41L++48PG3Ap8Wtt5W4d47YmXE8fPH7P9uZx5Y96s1ePhn5fTesTqWgAAAAArp2V8JAAAAAAAGTL0w8e2WdzusePdfjw0xx0rG/vT0AAAABk1rPvEOf0qf9Y/onFjj9kf0+blvF8sTEaiPpgAAAAAK6dlfCQAAAAAAHMzpTx+N6p9V+q6IisREezQAAAAAT8rN+nTpPXb59YdAAAAAArp2V8JAAAAAABkzpTxuPMz67LojUAAAAAAPHkZ64a9fefZ861py39dvloAAAAACunZXwkAAAAAAZM6h68bDOW/qnpX4fRiIiOjQAAAAAHlnxRkp/Mez5sxOO3pt0mGgAAAAAK6dlfCQAAAAABlYm+SKx7TPV9TFjjHSK/TsAAAAAAEfMw7j11jrtJE7aAAAAACunZXwkAAAAABlp1Cvh4tbtb56wsAAAAAAAZMbjT5eXH+lliv31YAAAAACunZXwkAAAAABkV/UyRT7fUx19OOsfUOwAAAAAABJzce6Tf6hHWejQAAAABXTsr4SAAAAAA9eJT1ZYv9S+iAAAAAAADjLT145r9vl69OS9fqdNAAAAAFdOyvhIAAAAAMtOoWcGusVvKsAAAAAAAB8vPX05pn7lyAAAAAK6dlfCQAAAAAc39o8vp8evpo9QAAAAAAAEHPjV8cw8AAAAABXTsr4SAAAAADm/tHl9esarHhoAAAAAAACLnx1olj2aAAAAArp2V8JAAAAABxf48vsV7Y8NAAAAAAAARc79iWPZoAAAACunZXwkAAAAAHF/jy+xXthoAAAAAAACLnfsSx7NAAAAAV07K+EgAAAAA4v8eX2K9seGgAAAAAAAIud+xLHs0AAAABXTsr4SAAAAADi/x5fYr2x4aAAAAAAAAi537EsezQAAAAFdOyvhIAAAAAyN2nURL0jh5L6n1RD6UdIhoAAAAAAACflYLZoj0zrSO3HyY46zvw4ifuNNAAAABXTsr4SAAAAAzU2tFY95fRwYYx0jp111ewAAAAAAAABraHl4YrH6kJoaAAAAK6dlfCQAAAAHWCN8inl9QAAAAAAAAAHlyI3imHza+8tAAAAFdOyvhIAAAADcM65NPL6oAAAAAAAAAPHkz6cUy+dX3loAAAArp2V8JAAAAAZv03i30+nht6sVZ+4dgAAAAAAAACTm5Pw9Hykj2aAAAAK6dlfCQAAAAGTG4U8PL1ms+0ey0AAAAAAAABlp1Ey+Zlv+rl9XwwAAAAFdOyvhIAAAAAyJml4tH2+lhyRkpFnoAAAAAAAACTl5vTHprPXfVJWNQ0AAAAFdOyvhIAAAAAyfZ6cfLOK+p7fp9GJiY6NAAAAAAAAeebJGOkzvrro+bMzkvN5+WgAAAAK6dlfCQAAAAAZMbU8XkdfRaesrQAAAAAAAc3vFKzafZ83NlnNk9+kT0Z7AAAAACunZXwkAAAAABzMddwt43IiY9Fukx0hUAAAAAAAy1orG5fOz55y21Ha84jUNAAAAAFdOyvhIAAAAAA561ncTrT6PGyevFWZnq9gAAAAAAQ8vLM2mlZ1pPEaaAAAAACunZXwkAAAAAAJ9npxcnoyTEz00+iAAAAAAOMlvRSZ/h8ybfqX9c/LQAAAAAFdOyvhIAAAAAAObR1iX0ePl/Vpt7AAAAAAIeXl9UxSvxPVPEahoAAAAACunZXwkAAAAAAB1gyfpZOva+lWdxEtAAAAAHlnyxjxz16/D53W1ptPy0AAAAAAV07K+EgAAAAAAMtG4U8XP+y0+NrQAAAAGWtFY3M6fMzZZy3/iGAAAAAAArp2V8JAAAAAAAHM7ifVX3hfxs8Xr6Z9491AAAAATOofP5Oeck+iva8YjTQAAAAAAV07K+EgAAAAAAA561mLR8dX0uPknLhi0+8vUAAABDy80+qaR8J4jTQAAAAAAFdOyvhIAAAAAAAMt2ys4U/8AiiFQAAAD5nJ/5NnIAAAAAAArp2V8JAAAAAAABlu2VfC/9cKwAAAHzOR/ybOQAAAAAABXTsr4SAAAAAAADLe0ruHGuPVQAAAA+dyo1mmXmAAAAAAAK6dlfCQAAAAAAAc9bXrEfb6mKnoxxV2AAAAk5uObY4mI67SVnbQAAAAAAFdOyvhIAAAAAAA5mfiOsq+Nx9fnbrM9YifhYAAAAMtWLRqXzc2GcV+nWriJaAAAAAACunZXwkAAAAAABzM7nULOPxfTq14iZ+FfsAAAAA5vSt66tG4fPzYLYp3GvS84nbQAAAAABXTsr4SAAAAAAMmdNpjvln8Y3C7Dxq4uvXf8vcAAAAAGTETGpR5+J1m1NzKXc1n026S6AAAAAAV07K+EgAAAAAzcEeq/SsTPhRh4kzq15nxMLK0rSOkRHh0AAAAAADyyYK5I9oifvSLJxr4p/HdoeXq10npLrYAAAAArp2V8JAAAAAZ6iItadRWVGPh2nraYmFdMNMfbXTsAAAAAAACY28MnFpfcxWN/aTJxr4+u9x/Dy3Me9ZbEw0AAAAV07K+EgAAAEzpzvc6h7U4uW/WdaUU4eOvWYnaitYrGoaAAAAAAAAAPO+GuTuhPk4Ufsjr/ACnvhyY+7WnEWh0AAACunZXwkAAAZMxBWL5J1SNvfHw5t1vuFdMVaRqI29AAAAAAAAAAAGTET7w8cvFpk+deEl+Nkp2xMw8vVqdT0l0AAArp2V8JAAAc7mZ1WJnwoxcSbflaZ8TCymKlPasQ7AAAAAAAAAAAACY37vHJxqXjpERP3pFkwXxT03aPDiLNAAFdOyvhIAAyZ06xYr5p6e0fa7Fx6Y43r8vl7AAAAAAAAAAAAAAyY3GpS5uJE9ccREpJiaTqzdgAK6dlfCQAHMz11CnBxJn8sm4lbFYrERHw0AAAAAAAAAAAAAAHnlw1yxqf9IMuC+Kfb8XETEtAFdOyvhIAMiJvOqxtbg40Ujdus/zCkAAAAAAAAAAAAAAAGWrFo1MRKHPxprM3puf4h4RbbQFdOyvhIAyInJaK1+flfhwVx1jpG3ttuzZtmzZtuzZs2zbdmzZs2zbdm2bbtm27Zs23Zs2zZs23Zs2bZtuzZs2bZtuzbNt2zbds2T1Rcjja/Oka/iE0T8T7ugV07K+EgOdTe3pr7voYMMY6+3V7AAAAAAAAAAAAAAAAACLk8fX508ynrO4aK6dlfCQczPWI+13GweiNz7qAAAAAAAAAAAAAAAAAAZMbjSDk4Jpb11+ZeUTuGq6dlfCQenGrFsk7jb6AAAAAAAAAAAAAAAAAAA5yViaTuN9HzNas1ZTsr4f/2Q=='}
                    />
                    <p />
                    <p style={{ fontSize: '24px' }} >{ele.name}</p>
                    <p>手机号: {ele.phoneNumber}</p>
                    <p>身份证号: {ele.idCard}</p>
                    <p>地址: {ele.address}</p>
                    <p>工作: {ele.work}</p>
                    <p>创建时间: {moment(ele.createTime).format('MM/DD/YYYY')}</p>
                  </Skeleton>
                </Card></Col>)
              })
          }
            </Row>
          </Modal>
        </div>
      </div>
    );
  }
}


export default UserInfoModal;
