import React, { Component } from 'react';
import { connect } from 'dva';
import { Cascader } from 'antd';
import ReactEcharts from 'echarts-for-react';
import { STRUCT } from '../../utils/NameSpace';
import actionHelper from '../../action/struct'

class TreePage extends Component {
    getOption = (tree) => {
      const option = {
        tooltip: {
          trigger: 'item',
          triggerOn: 'mousemove'
        },
        series: [
          {
            type: 'tree',
            // orient: 'vertical',
            // roam: true,
            data: tree,

            top: '1%',
            left: '7%',
            bottom: '1%',
            right: '20%',
            initialTreeDepth: 2,
            symbolSize: 7,
            nodePadding: 100,
            label: {
              normal: {
                position: 'left',
                verticalAlign: 'middle',
                align: 'right',
                fontSize: 9
              }
            },

            leaves: {
              label: {
                normal: {
                  position: 'right',
                  verticalAlign: 'middle',
                  align: 'left'
                }
              }
            },

            // expandAndCollapse: true,
            animationDuration: 550,
            animationDurationUpdate: 750
          }
        ]
      }

      return option
    }
    jilianChange = (value) => {
      const { tree, dispatch } = this.props;
      console.log(value, tree);
      dispatch(actionHelper.setData({
        showTree: this.formatStr(tree, value)
      }))
    }
    formatStr = (arr, len) => {
      let fin = arr;
      let show = arr;
      for (let i = 0; i < len.length; i++) {
        const elx = len[i];
        let s = show.find(ele => ele.id === elx);
        console.log(s)
        fin = [s]
        show = s.children
      }
      return fin
    }
    // Just show the latest item.
    render() {
      // const {} = this.props;
      console.log('tree: ', this.props.tree)
      return (
        <div style={{ marginTop: '24px' }}>
          <div>
            <Cascader
              style={{ width: '500px' }}
              options={this.props.tree}
              expandTrigger="hover"
              changeOnSelect
            //  displayRender={this.displayRender}
              onChange={this.jilianChange}
              placeholder="选择层级"
            />,
          </div>
          <ReactEcharts
            style={{ width: '100%', minHeight: '1200px', borderWidth: '1px', borderColor: 'red' }}
            option={this.getOption(this.props.showTree)}
            onEvents={this.oxEvent}
          />
        </div>
      );
    }
}

export const mapStateToProps = (state) => {
  const { tree, showTree } = state[STRUCT];
  return {
    tree,
    showTree
  }
}

export default connect(mapStateToProps)(TreePage);
