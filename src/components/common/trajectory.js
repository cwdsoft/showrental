import React, { Component } from 'react';
import { connect } from 'dva';
import { Modal, Row, Col, Timeline, Cascader } from 'antd';
import { routerRedux } from 'dva/router';
import RoutePath from '../../utils/RoutePath';
// import BMap from 'BMap'
// import ReactEcharts from 'echarts-for-react';
import { COMMON } from '../../utils/NameSpace';
import actionHelper from '../../action/common'


class Trajectory extends Component {
  state = {
    modalVisible: false,
    content: [{ label: '房东姓名', value: '小李' }, { label: '地址', value: '西安' }],
    count: 1
  }
  componentDidMount() {
    let BMap = window.BMap // 取出window中的BMap对象
    let map = new BMap.Map('allmap'); // 创建Map实例
    this.map = map;
    map.centerAndZoom(new BMap.Point(110.149280, 35.237520), 15); // 初始化地图,设置中心点坐标和地图级别
    map.addControl(new BMap.MapTypeControl()); // 添加地图类型控件
    map.setCurrentCity('渭南'); // 设置地图显示的城市 此项是必须设置的
    map.enableScrollWheelZoom(true); // 开启鼠标滚轮缩放d
  }

  componentWillReceiveProps(props) {
    let map = this.map
    let BMap = window.BMap // 取出window中的BMap对象
    map.clearOverlays()
    this.setState({
      count: 1
    })
    const cp = props.userPath.map((ele) => {
      return new BMap.Point(ele.y, ele.x)
    })
    setTimeout(() => {
      let convertor = new BMap.Convertor();
      for (let i = 0; i <= cp.length / 10; i++) {
        let v = []
        let chach = []
        if ((cp.length / 10).toFixed(0) === String(i)) {
          v = cp.slice(-(cp.length % 10))
          chach = props.userPath.slice(-(cp.length % 10))
        } else {
          v = cp.slice(i * 10, (i + 1) * 10)
          chach = props.userPath.slice(i * 10, (i + 1) * 10)
        }
        convertor.translate(v, 1, 5, (data) => {
          return this.translateCallback(data, map, chach)
        })
      }
    }, 1000)
  }

  setModalVisible = () => {
    this.setState({
      modalVisible: false
    })
  }
  translateCallback = (data, map, oldData) => {
    if (data.status === 0) {
      let BMap = window.BMap // 取出window中的BMap对象
      for (let i = 0; i < data.points.length; i++) {
        let marker = new BMap.Marker(data.points[i])
        marker.info = oldData[i]
        marker.address = oldData[i].address
        let label = new BMap.Label(this.state.count, { offset: new BMap.Size(3, 0) });
        marker.setLabel(label);
        marker.addEventListener('onclick', (e) => {
          if (e.target.address) {
            this.jump(e.target.address)
          }
          console.log('onclick ：', e.target)
        })
        // marker.addEventListener('click', this.onMarkerClick)
        map.setCenter(data.points[i]);
        this.setState({
          count: this.state.count + 1
        })
        map.addOverlay(marker);
      }
      //   let sy = new BMap.Symbol();
      //   let icons = new BMap.IconSequence(sy, '10', '30');
      let polyline = new BMap.Polyline(data.points, {
        enableEditing: false, // 是否启用线编辑，默认为false
        enableClicking: false, // 是否响应点击事件，默认为true
        strokeWeight: '4', // 折线的宽度，以像素为单位
        strokeOpacity: 0.8, // 折线的透明度，取值范围0 - 1
        strokeColor: 'red', // 折线颜色
        // icons: icons
      });
      map.addOverlay(polyline);
    }
  }
  jilianChange = (value) => {
    if (value.length >= 0) {
      this.props.dispatch(actionHelper.getUserPath({ ZuKeId: value[value.length - 1] }))
    }
  }
  jump = (fangwuid) => {
    this.props.dispatch(routerRedux.push(RoutePath.HOUSE + '?query=' + fangwuid));
  }
  render() {
    const { userPath } = this.props;
    console.log(userPath)
    return (
      <div>
        <Modal
          title="房屋信息"
          style={{ top: 20 }}
          visible={this.state.modalVisible}
          onOk={this.setModalVisible}
          onCancel={this.setModalVisible}
          footer={null}
        >
          {
          this.state.content.map((ele, i) => <p key={i} >{ele.label}: {ele.value}</p>)
        }

        </Modal>

        <Row gutter={24} >
          <Col span={8} >
            <Cascader
              style={{ width: '100%' }}
              options={this.props.tree}
              expandTrigger="hover"
              changeOnSelect
              onChange={this.jilianChange}
              placeholder="选择层级"
            />
            <div style={{ height: '24px' }} />
            <Timeline>
              {
               this.props.userPath.map((ele, i) =>
                 <Timeline.Item key={i} > {i + 1}. {ele.inDate}入住{ele.address}</Timeline.Item>)
              }
            </Timeline>
          </Col>
          <Col span={16} >
            <div
              id="allmap"
              style={{
                width: '100%',
                height: '100vh'
              }}
            />
          </Col>
        </Row>
      </div>
    );
  }
}

export const mapStateToProps = (state) => {
  const { tree, userPath } = state[COMMON];
  return {
    tree,
    userPath
  }
}

export default connect(mapStateToProps)(Trajectory);
