import { DatePicker, Row, Col } from 'antd';
import moment from 'moment'
import React, { Component } from 'react';

function disabledDate(current, next, isFromDate = false) {
  const nx = moment.isMoment(next) ? next : moment(next)
  if (isFromDate) {
    return current && current < nx.endOf('day');
  }
  return current && current.endOf('day') > nx;
}

const width100 = {
  width: '100%'
}

/**
 * 两个datePicker在一起的组件；
 * 接受一个value 为{ from: mement(),to: moment() } 和一个 onChange；
 * 不允许 from小于to；
 * @class DoubleDatePicker
 * @extends {React.Component}
 * @prop {Object} value 必须包含 from, to 两个moment对象
 * @prop {Function} onChange 外层传递的事件
 *
 */
class DoubleDatePicker extends Component {
  constructor(props) {
    super(props);
    let from = this.props.value.from;
    let to = this.props.value.to;
    this.state = {
      from: from,
      to: to,
    }
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    let from = nextProps.value.from;
    let to = nextProps.value.to;
    if (from !== prevState.from || to !== prevState.to) {
      return {
        from: from,
        to: to
      }
    }
    return null
  }

  changeValue = (value, dateString, isFrom) => {
    let valueFinal = {};
    if (isFrom) {
      valueFinal = { from: value, to: this.state.to }
      this.setState({ from: value })
    } else {
      valueFinal = { from: this.state.from, to: value }
      this.setState({ to: value })
    }
    this.triggerChange(valueFinal)
  }
  triggerChange = (value) => {
    const onChange = this.props.onChange;
    if (onChange) {
      onChange(value)
    }
  }
  render() {
    const { from, to } = this.state;
    return (
      <Row gutter={8} type="flex" justify="space-between">
        <Col span={12}>
          <DatePicker
            style={width100}
            value={from}
            onChange={(date, dateString) => { this.changeValue(date, dateString, true) }}
            disabledDate={(current) => { return disabledDate(current, to) }}
            placeHolder="开始日期"
          />
        </Col>
        <Col span={12}>
          <DatePicker
            style={width100}
            value={to}
            onChange={(date, dateString) => { this.changeValue(date, dateString, false) }}
            disabledDate={(current) => { return disabledDate(current, from, true) }}
          />
        </Col>
      </Row>
    );
  }
}

export function validatDate(rule, value, callback) {
  const { from, to } = value;
  if (!from || !to) {
    callback()
    return
  }
  if (moment(to).isBefore(from)) {
    callback('from date must less to date!')
    return
  }
  callback()
}
export default DoubleDatePicker;
