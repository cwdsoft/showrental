import React, { Component } from 'react';
import { connect } from 'dva';
import { Tree, Button, Switch, Alert } from 'antd';
import { COMMON } from '../../utils/NameSpace';
import actionHelper from '../../action/common'
import { isNoData } from '../../utils/Util';

const { TreeNode } = Tree;

class NewsManage extends Component {
  state = {
    gData: [],
    expandedKeys: ['0'],
    lock: false
  }
  static getDerivedStateFromProps(props, state) {
    if (state.lock) {
      return null
    }
    return { gData: props.newsList, lock: true }
  }
  onDrop = (info) => {
    if (!info.dragNode) {
      return
    }
    const dropKey = info.node.props.eventKey;
    const dragKey = info.dragNode.props.eventKey;
    const dropPos = info.node.props.pos.split('-');
    const dropPosition = info.dropPosition - Number(dropPos[dropPos.length - 1]);

    const loop = (data, key, callback) => {
      data.forEach((item, index, arr) => {
        if (item.key === key) {
          return callback(item, index, arr);
        }
        if (item.children) {
          return loop(item.children, key, callback);
        }
      });
    };
    const data = [...this.state.gData];

    // Find dragObject
    let dragObj;
    loop(data, dragKey, (item, index, arr) => {
      arr.splice(index, 1);
      dragObj = item;
    });

    let ar;
    let i;
    loop(data, dropKey, (item, index, arr) => {
      ar = arr;
      i = index;
    });
    console.log('ar', ar)
    if (dropPosition === -1) {
      ar.splice(i, 0, dragObj);
    } else {
      ar.splice(i + 1, 0, dragObj);
    }

    this.setState({
      gData: data,
    });
  }
  save = () => {
    // let finValue = this.state.gData.filter(ele => ele.visiable)
    let finValue = this.state.gData
    this.props.dispatch(actionHelper.saveNews(finValue))
    console.log(finValue)
  }
  switchChange = (key) => {
    console.log(key)
    let v = this.state.gData

    v.forEach((ele) => {
      if (ele.key === key) {
        ele.visiable = !ele.visiable
      }
      return ele
    });

    this.setState({
      gData: v
    })
  }
  render() {

    if (isNoData(this.state.gData)) {
      return (<Alert
        message="请注意"
        description="暂无可用公告"
        type="warning"
        showIcon
      />)
    }

    const loop = data => data.map((item) => {
      if (item.children && item.children.length) {
        return <TreeNode key={item.key} title={item.title}>{loop(item.children)}</TreeNode>;
      }
      return (<TreeNode
        key={item.key}
        title={<div style={{ display: 'flex', justifyContent: 'space-between' }} >
          <span>{item.title} - {item.sender} - {item.date}</span>
          <Switch
            checked={item.visiable}
            onClick={() => { this.switchChange(item.key) }}
            checkedChildren="展示本条"
            unCheckedChildren="不展示本条"
          /></div>}
      />);
    });
    return (
      <div>
        <Tree
          className="draggable-tree"
          defaultExpandedKeys={this.state.expandedKeys}
          draggable
          blockNode
          onDrop={this.onDrop}
        >
          {loop(this.state.gData)}
        </Tree>
        <div style={{ height: '38px' }} />
        <Button style={{ width: '100px' }} type="primary" onClick={this.save} block>保存修改</Button>
        <div style={{ height: '18px' }} />
        <Alert
          message="小提示"
          description="拖拽内容以调整公告显示顺序, 点击按钮切换是否展示本条内容"
          type="info"
          showIcon
        />
      </div>
    );
  }
}

export const mapStateToProps = (state) => {
  const { newsList } = state[COMMON];
  return {
    newsList
  }
}

export default connect(mapStateToProps)(NewsManage);
