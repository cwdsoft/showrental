import React from 'react';

class TextEllipse extends React.PureComponent {
  render() {
    const { children, title, width } = this.props;
    return (<div
      style={{ maxWidth: width }}
      className="text-ellipsis"
      title={title || ''}
    >{children}</div>)
  }
}

export default TextEllipse
