import React, { Component } from 'react';
import { connect } from 'dva';
import ReactEcharts from 'echarts-for-react';
import { COMMON } from '../../utils/NameSpace';
// import actionHelper from '../../action/common'


class ageBing extends Component {
    // state = {
    //   treeData: td
    // }
    getOption = () => {
      const option = {
        title: {
          text: '合阳县流动人口性别分布饼图',
          subtext: '截至于2019年4月',
          x: 'center'
        },
        tooltip: {
          trigger: 'item',
          formatter: '{a} <br/>{b} : {c} ({d}%)'
        },
        legend: {
          orient: 'vertical',
          left: 'left',
          data: ['男', '女']
        },
        series: [
          {
            name: '性别分布',
            type: 'pie',
            radius: '55%',
            center: ['50%', '60%'],
            data: [
              {
                value: this.props.sexPie.man,
                name: '男'
              },
              {
                value: this.props.sexPie.woman,
                name: '女'
              }
            ],
            itemStyle: {
              emphasis: {
                shadowBlur: 10,
                shadowOffsetX: 0,
                shadowColor: 'rgba(0, 0, 0, 0.5)'
              }
            }
          }
        ]
      };


      return option
    }
    render() {
      const { sexPie } = this.props;
      console.log(sexPie)
      return (
        <div style={{ marginTop: '24px' }}>
          <ReactEcharts option={this.getOption()} />
        </div>
      );
    }
}

export const mapStateToProps = (state) => {
  const { sexPie } = state[COMMON];
  return {
    sexPie
  }
}

export default connect(mapStateToProps)(ageBing);
