import React, { Component } from 'react';
import { connect } from 'dva';
import ReactEcharts from 'echarts-for-react';
import { COMMON } from '../../utils/NameSpace';
// import actionHelper from '../../action/common'


class ageBing extends Component {
    // state = {
    //   treeData: td
    // }
    getOption = () => {
      const { baseInfo } = this.props;
      let {
        star1Count,
        star2Count,
        star3Count,
        star4Count,
        star5Count,
      } = baseInfo
      const option = {
        title: {
          text: '合阳县房屋安全分布饼图',
          subtext: '截至于2019年5月',
          x: 'center'
        },
        tooltip: {
          trigger: 'item',
          formatter: '{a} <br/>{b} : {c} ({d}%)'
        },
        legend: {
          orient: 'vertical',
          left: 'left',
          data: ['不合格房屋', '基本合格房屋', '合格房屋', '良好房屋', '优秀房屋']
        },
        series: [
          {
            name: '合阳县房屋安全分布饼图',
            type: 'pie',
            radius: '55%',
            center: ['50%', '60%'],
            data: [
              {
                value: star1Count,
                name: '不合格房屋'
              },
              {
                value: star2Count,
                name: '基本合格房屋'
              },
              {
                value: star3Count,
                name: '合格房屋'
              },
              {
                value: star4Count,
                name: '良好房屋'
              },
              {
                value: star5Count,
                name: '优秀房屋'
              },
            ],
            itemStyle: {
              emphasis: {
                shadowBlur: 10,
                shadowOffsetX: 0,
                shadowColor: 'rgba(0, 0, 0, 0.5)'
              }
            }
          }
        ]
      };


      return option
    }
    render() {
      return (
        <div style={{ marginTop: '24px' }}>
          <ReactEcharts option={this.getOption()} />
        </div>
      );
    }
}

export const mapStateToProps = (state) => {
  const { baseInfo } = state[COMMON];
  return {
    baseInfo
  }
}

export default connect(mapStateToProps)(ageBing);
