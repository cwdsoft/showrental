import React, { Component } from 'react';
import { connect } from 'dva';
import { routerRedux } from 'dva/router';
import {
  Form, Icon, Input, Button, message,
} from 'antd';
import { COMMON } from '../../utils/NameSpace';
import RoutePath from '../../utils/RoutePath';
import actionHelper from '../../action/common'


class Login extends Component {
    handleSubmit = (e) => {
      e.preventDefault();
      const that = this;
      this.props.form.validateFields((err, values) => {
        if (!err) {
          console.log('success', values)
          that.props.dispatch(actionHelper.checkLogin({
            idcard: values.password,
            name: values.userName
          })).then((res) => {
            console.log('------------', res)
            if (res) {
              message.success(values.userName + '登陆成功!');
              this.props.dispatch(routerRedux.push(RoutePath.VIEW));
            } else {
              message.error('账号或密码错误!');
            }
          })
        }
      });
    }
    render() {
      const { getFieldDecorator } = this.props.form;

      const { sexPie } = this.props;
      console.log(sexPie)
      return (
        <div className="loginbg">
          <div>
            <div style={{ marginTop: '70%', display: 'flex', justifyContent: 'space-around' }} >
              <img style={{ width: '150px', height: '150px' }} alt="jinghui" src="./loc/image/jinghui.png" />
            </div>
            <div style={{
            width: '300px',
            marginTop: '50px'
          }}
            >
              <Form onSubmit={this.handleSubmit} className="login-form">
                <Form.Item>
                  {getFieldDecorator('userName', {
                      rules: [{ required: true, message: 'Please input your username!' }],
                    })(
                      <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="用户名" />
                    )}
                </Form.Item>
                <Form.Item>
                  {getFieldDecorator('password', {
                      rules: [{ required: true, message: 'Please input your Password!' }],
                    })(
                      <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="密码" />
                    )}
                </Form.Item>
                <Form.Item>
                  <Button style={{ width: '100%' }} type="primary" htmlType="submit" className="login-form-button">
                登录
                  </Button>
                </Form.Item>
              </Form>
            </div>
          </div>
        </div>
      );
    }
}

const WrappedNormalLoginForm = Form.create({ name: 'normal_login' })(Login);

export const mapStateToProps = (state) => {
  const { sexPie } = state[COMMON];
  return {
    sexPie
  }
}

export default connect(mapStateToProps)(WrappedNormalLoginForm);
