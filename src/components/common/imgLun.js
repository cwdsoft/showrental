import React, { Component } from 'react';
import { Modal } from 'antd';
import { isNoData } from '../../utils/Util';

class ImgModal extends Component {
    state = { visible: false }

  showModal = () => {
    this.setState({
      visible: true,
    });
  }

  handleOk = (e) => {
    console.log(e);
    this.setState({
      visible: false,
    });
  }

  handleCancel = (e) => {
    console.log(e);
    this.setState({
      visible: false,
    });
  }
  render() {
    const { imgBox = [], children } = this.props;
    return (
      <div>
        <div>
          {
            isNoData(imgBox) ?
            '未录入'
             :
            <a onClick={this.showModal} style={{ textDecoration: 'underline' }} >{children}</a>
          }

          <Modal
            title="详图"
            visible={this.state.visible}
            onOk={this.handleOk}
            onCancel={this.handleCancel}
            footer={null}
          >
            {
                imgBox.map((ele, i) =>
                (<div key={i} >
                  <img
                    style={{ width: '100%', minHeight: '400px' }}
                    alt="pic"
                    src={ele}
                  />
                </div>))
               }
          </Modal>
        </div>
      </div>
    );
  }
}


export default ImgModal;
