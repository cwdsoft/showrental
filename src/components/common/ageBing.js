import React, { Component } from 'react';
import { connect } from 'dva';
import ReactEcharts from 'echarts-for-react';
import { COMMON } from '../../utils/NameSpace';
// import actionHelper from '../../action/common'

class ageBing extends Component {
    getOption = () => {

      const option = {
        title: {
          text: '合阳县流动人口年龄分布饼图',
          subtext: '截至于2019年4月',
          x: 'center'
        },
        tooltip: {
          trigger: 'item',
          formatter: '{a} <br/>{b} : {c} ({d}%)'
        },
        legend: {
          orient: 'vertical',
          left: 'left',
          data: ['创微达']
        },
        series: [
          {
            name: '年龄分布',
            type: 'pie',
            radius: '55%',
            center: ['50%', '60%'],
            data: this.props.renPie,
            itemStyle: {
              emphasis: {
                shadowBlur: 10,
                shadowOffsetX: 0,
                shadowColor: 'rgba(0, 0, 0, 0.5)'
              }
            }
          }
        ]
      };


      return option
    }
    render() {
      // const {} = this.props;
      return (
        <div style={{ marginTop: '24px' }}>
          <ReactEcharts option={this.getOption()} />
        </div>
      );
    }
}

export const mapStateToProps = (state) => {
  const { renPie } = state[COMMON];
  return {
    renPie
  }
}

export default connect(mapStateToProps)(ageBing);
