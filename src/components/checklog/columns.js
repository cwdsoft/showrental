export const columns = [
  {
    title: '房屋',
    dataIndex: 'fangWuAddress',
    key: 'fangWuAddress',
    visible: true,
    // renderFn: 'renderLink',
  },
  // {
  //   title: '房号',
  //   dataIndex: 'fangHaoName',
  //   key: 'fangHaoName',
  //   visible: true,
  //   // renderFn: 'renderLink',
  // },
  {
    title: '检查者',
    dataIndex: 'handlerName',
    key: 'handlerName',
    visible: true,
    filterable: true
    // renderFn: 'renderLink',
  },
  {
    title: '检查日期',
    dataIndex: 'createTime',
    key: 'createTime',
    visible: true,
    // renderFn: 'renderLink',
  },
  {
    title: '检查结果',
    dataIndex: 'problem',
    key: 'problem',
    visible: true,
    renderFn: 'renderResult',
  },
]

