import React, { Component } from 'react';
import { Form, Button, Cascader, Select } from 'antd';
import { connect } from 'dva';
import { isNoData } from '../../utils/Util';
import { COMMON } from '../../utils/NameSpace';
import actionHelper from '../../action/common'

const { Option } = Select;
const formItemLayout = 'vertical';

class RichTextPage extends Component {
  getRoomOption = () => {
    return this.props.fangWuList.map((ele) => {
      return <Option key={ele.id} value={ele.id}>{ele.address}</Option>
    })
  }
  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
        let [fangwuId, HandlerId] = [-1, -1];
        if (values.HandlerId && values.HandlerId.length >= 1) {
          HandlerId = values.HandlerId[values.HandlerId.length - 1]
        }
        if (!isNoData(values.fangwuId)) {
          fangwuId = values.fangwuId
        }
        let fin = {
          fangwuId: fangwuId,
          HandlerId: HandlerId
        }
        console.log('Received values of form: ', fin);

        this.props.dispatch(actionHelper.getRoomCheckLog(fin)).then((result) => {
          if (result) {
            // notification.success({
            //   message: '恭喜你',
            //   description: '发布成功!',
            // });
          }
        })
      }
    });
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <div>
        <Form layout="inline" onSubmit={this.handleSubmit} >
          <Form.Item
            label="房屋"
            {...formItemLayout}
          >
            {getFieldDecorator('fangwuId')(
              <Select
                allowClear
                style={{ width: 200 }}
              >
                {
                  this.getRoomOption()
                }
              </Select>
          )}
          </Form.Item>
          <Form.Item
            label="检查者"
            {...formItemLayout}
          >
            {getFieldDecorator('HandlerId')(
              <Cascader
                style={{ width: '300px' }}
                options={this.props.mjWgyTree}
                expandTrigger="hover"
                changeOnSelect
                placeholder="选择层级"
              />
          )}
          </Form.Item>
          <Form.Item>
            <Button type="primary" htmlType="submit">查询</Button>
          </Form.Item>
        </Form>
      </div>
    );
  }
}

export const mapStateToProps = (state
) => {
  const { fangWuList, mjWgyTree } = state[COMMON];
  return {
    fangWuList, mjWgyTree
  }
}

const RichTextPageWrapp = Form.create()(RichTextPage);


export default connect(mapStateToProps)(RichTextPageWrapp);
