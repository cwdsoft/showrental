import React, { Component } from 'react';
import { Row, Col } from 'antd';
import { connect } from 'dva';
import { COMMON } from '../../utils/NameSpace';
// import actionHelper from '../../action/common'
import AgeBing from '../common/ageBing'
import SexBing from '../common/sexBing'
import FangwuBind from '../common/fangwuBind'

class ViewMainPage extends Component {
  render() {
    // const {} = this.props;
    return (
      <div>
        <Row>
          <Col span={12} ><AgeBing /></Col>
          <Col span={12} ><SexBing /></Col>
          <Col span={12} ><FangwuBind /></Col>
        </Row>
      </div>
    );
  }
}

export const mapStateToProps = (state) => {
  const { selectedKeys } = state[COMMON];
  return {
    selectedKeys
  }
}

export default connect(mapStateToProps)(ViewMainPage);
