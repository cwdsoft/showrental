import React, { Component } from 'react';
import { Form, Input, Button, message, notification, Popconfirm } from 'antd';
import E from 'wangeditor'
import { connect } from 'dva';
import './index.css';
import { isNoData } from '../../utils/Util';
import { COMMON } from '../../utils/NameSpace';
import actionHelper from '../../action/common'
import UploadPic from './uploadPic'

const { TextArea } = Input;
const formItemLayout = null;
const buttonItemLayout = null;

class RichTextPage extends Component {
  constructor(props, context) {
    super(props, context);
    this.editorElem = React.createRef();
    this.state = {
      editorContent: ''
    }
  }

  componentDidMount() {
    if (this.props.isnews) {
      const editor = new E(this.editorElem.current)
      editor.customConfig.menus = [
        'bold',
        'italic',
        'link',
        'underline',
        'image',
      ]
      editor.customConfig.uploadImgShowBase64 = true
      // editor.customConfig.uploadImgServer = '/upload' // 上传的服务器地址
      // 使用 onchange 函数监听内容的变化，并实时更新到 state 中
      editor.customConfig.onchange = (html) => {
        this.setState({
          editorContent: html
        })
      }
      editor.create()
    }
  }

  // getBase64Image = (file) => {
  //   let fd = new FileReader();
  //   let fileType = file.type;
  //   fd.readAsDataURL(file)
  //   return new Promise((resolve, reject) => {
  //     fd.onload = function () {
  //       if (/^image\/[jpeg|png|jpg|gif]/.test(fileType)) {
  //         resolve(this.result);
  //         return false
  //       }
  //     }
  //   })
  // }

  handleSubmit = (e) => {
    e.preventDefault();
    const { isnews, userInfo } = this.props;
    this.props.form.validateFields((err, values) => {
      if (!err) {
        if (isNoData(this.state.editorContent) && isnews) {
          message.warning('请填充内容');
          return false
        }
        let finContent = this.state.editorContent
        let fin = {}
        if (isnews) {
          fin = {
            mainTitle: values.mainTitle,
            viceTitle: values.viceTitle,
            content: finContent,
            sender: userInfo.id,
            pic: values.pic[0].response.data
          }
        } else {
          fin = {
            PostUserId: 0,
            ReadLevel: 0,
            MessageType: 1,
            Content: values.content,
            Title: values.mainTitle,
          }
        }
        this.props.dispatch(actionHelper[this.props.act](fin)).then((result) => {
          if (result) {
            notification.success({
              message: '恭喜你',
              description: '发布成功!',
            });
          }
        })
      }
    });
  }
  render() {
    const { isnews } = this.props
    const { getFieldDecorator } = this.props.form;
    return (
      <div>
        <Form layout="vertical" onSubmit={this.handleSubmit} >
          <Form.Item
            label="主标题"
            {...formItemLayout}
          >
            {getFieldDecorator('mainTitle', {
            rules: [{
              required: true,
              message: '主标题必填',
            }],
          })(
            <Input placeholder="输入主标题" />
          )}
          </Form.Item>
          {
          isnews && <Form.Item
            label="副标题"
            {...formItemLayout}
          >
            {getFieldDecorator('viceTitle', {
          rules: [{
            required: true,
            message: '副标题必填',
          }],
        })(
          <Input placeholder="输入副标题" />
        )}
          </Form.Item>
          }
          <Form.Item
            label="内容"
            {...formItemLayout}
          >
            {
           isnews ? <div ref={this.editorElem} style={{ textAlign: 'left' }} /> : getFieldDecorator('content', {
            rules: [{
              required: true,
              message: '内容必填',
            }],
          })(
            <TextArea placeholder="输入内容" autosize={{ minRows: 2, maxRows: 6 }} />
          )}
          </Form.Item>
          {
           isnews && <UploadPic {...this.props} />
          }
          <Form.Item {...buttonItemLayout}>
            <Popconfirm placement="right" title="确定要发布吗" onConfirm={this.handleSubmit} okText="确定" cancelText="再看看">
              <Button type="primary" htmlType="submit">立即发布</Button>
            </Popconfirm>
          </Form.Item>
        </Form>
      </div>
    );
  }
}

export const mapStateToProps = (
  state
) => {
  const { userInfo } = state[COMMON];
  return {
    userInfo
  }
}

const RichTextPageWrapp = Form.create()(RichTextPage);


export default connect(mapStateToProps)(RichTextPageWrapp);
