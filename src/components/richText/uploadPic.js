import React, { Component } from 'react';
import { Form, Upload, Icon, Modal } from 'antd';
import { connect } from 'dva';
import './index.css';
// import { isNoData } from '../../utils/Util';
// import { COMMON } from '../../utils/NameSpace';
// import actionHelper from '../../action/common'
import { API } from '../../utils/Constant'

const formItemLayout = null;

class RichTextPage extends Component {
    state = {
      previewVisible: false,
      previewImage: '',
      fileList: [],
    };

    handleChange = ({ fileList }) => this.setState({ fileList })
    normFile = (e) => {
      if (Array.isArray(e)) {
        return e;
      }
      return e && e.fileList;
    }
    render() {
      const { isnews } = this.props
      const { getFieldDecorator } = this.props.form;
      const { previewVisible, previewImage, fileList } = this.state;
      const uploadButton = (
        <div>
          <Icon type="plus" />
          <div className="ant-upload-text">Upload</div>
        </div>
      );
      return (
        <div>
          <Modal visible={previewVisible} footer={null} onCancel={this.handleCancel}>
            <img alt="example" style={{ width: '100%' }} src={previewImage} />
          </Modal>
          {
          isnews && <Form.Item
            label="上传封面图片"
            {...formItemLayout}
          >
            {getFieldDecorator('pic', {
                // valuePropName: 'fileList',
                getValueFromEvent: this.normFile,
                rules: [{
                    required: true,
                    message: '封面图片必填',
                }],
        })(
          <Upload
            name="pic"
            // action="https://192.168.6.106:44337/api//FileUpdate/UpdateFile"
            action={API.UpdateFile}
            listType="picture-card"
            fileList={fileList}
            onChange={this.handleChange}
            showUploadList={
                {
                    showPreviewIcon: false
                }
            }
          >
            {fileList.length >= 1 ? null : uploadButton}
          </Upload>
        )}
          </Form.Item>
          }
        </div>
      );
    }
}

export const mapStateToProps = (
//   state
) => {
//   const { } = state[COMMON];
  return {

  }
}

const RichTextPageWrapp = Form.create()(RichTextPage);


export default connect(mapStateToProps)(RichTextPageWrapp);
