import React, { Component } from 'react'
import { Layout, Menu, Icon, Breadcrumb, Badge, Avatar, Dropdown, Card } from 'antd';
import { connect } from 'dva';
import { Route, Switch, withRouter, routerRedux } from 'dva/router';
import { COMMON } from '../../utils/NameSpace';
import actionHelper from '../../action/common'
import RoutePath from '../../utils/RoutePath';

const {
  Header, Content, Footer, Sider,
} = Layout;
const SubMenu = Menu.SubMenu;

class BaseLayout extends Component {
    state = {
      collapsed: false,
    };
    onCollapse = (collapsed) => {
      this.setState({ collapsed });
    }
    getMenu = () => {
      const { userInfo: { name, headpath } } = this.props
      return (
        <div style={{ zIndex: '99999' }} >
          <Card
            title={name}
            extra={<a onClick={this.logout} >登出</a>}
            style={{ width: 250 }}
          >
            {
            String(headpath) !== 'null' && <img
              style={{ width: '100%', height: '250px' }}
              alt="pic"
              src={headpath}
            />
          }
            {/* <p>详细描述</p> */}
          </Card>
        </div>)
    }
    logout = () => {
      this.props.dispatch(actionHelper.setData({
        hasUserInfo: false,
        userInfo: null
      }))
      let storage = window.sessionStorage;
      storage.clear();
      this.props.dispatch(routerRedux.push(RoutePath.LOGIN));

    }
    handleClick = (e) => {
      console.log('click', e.key);
      const { dispatch } = this.props;
      dispatch(actionHelper.setData({
        selectedKeys: e.key,
      }))
      if (RoutePath[e.key]) {
        dispatch(routerRedux.push(RoutePath[e.key]));
      }
    }
    render() {
      const {
        selectedKeys = 'VIEW',
        itemBox, projectName,
        routes,
        duizhao,
        isLogin,
        userInfo,
        hasUserInfo
      } = this.props;

      console.log(userInfo, 'userInfo: ')
      return (
        <Layout>
          <Header style={{ background: '#002140', padding: 0 }} >
            <div style={{ margin: '0 50px', display: 'flex', justifyContent: 'space-between' }} >
              <div style={{ color: '#FFF', fontSize: '32px' }} >{projectName}</div>
              <div>
                {
                  hasUserInfo && <div>
                    <span style={{ marginRight: 24 }}>
                      <Badge count={0}><Avatar src={userInfo.headpath} icon="user" /></Badge>
                    </span>
                    <span style={{ color: '#FFF' }} >
                      <Dropdown overlay={this.getMenu()} >
                        <span>{userInfo.name} <Icon type="down" /></span>
                      </Dropdown>
                    </span>
                  </div>
                }
              </div>
            </div>
          </Header>
          <Layout style={{ minHeight: '100vh' }} >

            {
            !isLogin && <Sider
              collapsible
              collapsed={this.state.collapsed}
              onCollapse={this.onCollapse}
            >
              <div className="logo" />
              <Menu
                theme="dark"
              // defaultSelectedKeys={['1']}
                selectedKeys={[selectedKeys]}
                mode="inline"
                onClick={this.handleClick}
              >
                {
              itemBox.map((ele) => {
                if (!ele.children) {
                  return (<Menu.Item key={ele.key}>
                    <Icon type={ele.icon} />
                    <span>{ele.title}</span>
                  </Menu.Item>)
                }
                const inner = ele.children.map(eele =>
                  <Menu.Item key={eele.key}>{eele.title}</Menu.Item>)
                return (<SubMenu
                  key={ele.key}
                  title={<span><Icon type={ele.icon} /><span>{ele.title}</span></span>}
                >
                  {
                inner
               }
                </SubMenu>)
              })
            }
              </Menu>
            </Sider>
          }

            <Layout>
              <Content style={{ margin: '0 16px' }}>
                <Breadcrumb style={{ margin: '16px 0' }}>
                  {
                !isLogin && duizhao[selectedKeys].map((ele, i) =>
                  <Breadcrumb.Item key={i}>{ele.title}</Breadcrumb.Item>)
                }
                </Breadcrumb>
                <div style={{ padding: !isLogin ? 24 : 1, margin: '16px 0', background: '#fff', minHeight: 360 }}>
                  <Switch>
                    {
                      routes.map((route) => {
                        return (<Route
                          key={route.path}
                          path={route.path}
                          exact
                          component={route.component}
                          {...route.options}
                        />)
                      })
                    }
                  </Switch>
                </div>
              </Content>
              <Footer style={{ textAlign: 'center' }}>
              合阳公安局版权所有 ©2019 技术支持: 西安创微达网络科技有限公司
              </Footer>
            </Layout>
          </Layout>
        </Layout>
      );
    }
}

export const mapStateToProps = (state) => {
  const { itemBox, projectName, duizhao,
    selectedKeys, isLogin, userInfo, hasUserInfo } = state[COMMON];
  return {
    itemBox,
    projectName,
    duizhao,
    selectedKeys,
    isLogin,
    userInfo,
    hasUserInfo
  }
}

export default withRouter(connect(mapStateToProps)(BaseLayout));
