import React, { Component } from 'react'
import ExportJsonExcel from 'js-export-excel'
import {
  Table, Select, BackTop, Spin, Row, Col, Button
  // Badge
} from 'antd';
import { connect } from 'dva';
import Highlighter from 'react-highlight-words';
import { COMMON } from '../../utils/NameSpace';
import { isNoData } from '../../utils/Util'
// import css from './index.css'
import TextEllipse from '../common/textEllipse';
import ImgLun from '../common/imgLun';
import Personinfo from '../common/personinfo';
import { columns as constColumns, houseType } from './columns.js'
import actionhelper from '../../action/common';
import { getExcelData } from '../utils'

const Option = Select.Option;

class HouseTable extends Component {
  getColumns = (columns) => {
    const TableColumns = columns.filter(ele => ele.visible).map((item) => {
      // if (!isNoData(item.sortFn)) { this[item.sortFn](item, item.dataIndex) }
      if (item.filterable) { this.filterColumn(item, item.dataIndex) }
      if (isNoData(item.renderFn)) { this.renderColumn(item); return item; }
      this[item.renderFn](item, item.dataIndex);
      return item;
    });
    return TableColumns;
  }

  excelDownload = () => {
    const { dataSource } = this.props;
    let columns = this.getColumns(constColumns)
    let ds = dataSource

    let option = {};
    option.fileName = '房屋表'
    option.datas = getExcelData(columns, ds)
    let toExcel = new ExportJsonExcel(option);
    toExcel.saveExcel();
  }
  handleChange = (value) => {
    this.props.dispatch(actionhelper.setData({
      currentStar: value,
      problemRoomDataSource:
      this.props.originDataSource.filter(ele => String(ele.star) === String(value))
    }))
  }
  filterColumn = (column, dataIndex) => {
    const { dataSource } = this.props;
    let Box = new Set();
    dataSource.forEach((element) => {
      if (isNoData(element[dataIndex])) return;
      Box.add(element[dataIndex])
    });
    column.filters = Array.from(Box, (ele) => { return { text: ele, value: ele } })
    column.onFilter = (value, record) => {
      if (isNoData(record[dataIndex])) return false;
      return record[dataIndex].toString() === value.toString()
    }
  }
  renderColumn = (column) => {
    column.render = (text) => {
      if (!text) {
        return text
      }
      return (
        <TextEllipse><Highlighter
          highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
          searchWords={[this.props.searchText]}
          autoEscape
          textToHighlight={text.toString()}
        /></TextEllipse>
      );
    }
  }
  renderType = (column) => {
    column.render = (text, record) => {
      let txt = houseType[record.fangWuType]
      if (!txt) {
        return txt
      }
      return (
        <TextEllipse>{txt}</TextEllipse>
      );
    }
  }
  renderStar = (column) => {
    column.render = (text, record) => {
      let count = Array.from(Array(record.star), () => 1);
      return (
        <div>
          {
          count.map((ele, i) => <img key={i} style={{ width: '20px', height: '20px' }} alt="star" src="./loc/image/star.png" />)
        }
        </div>
      );
    }
  }
  renderInfoModal = (column) => {
    column.render = (text, record) => {
      if (column.dataIndex === 'suizhuCount') {
        if (String(text) === '0') {
          return text
        }
        if (!text) {
          return text
        }
        return (
          <Personinfo
            clickBefore={() => {
              this.props.dispatch(actionhelper.getAllSuicong({ ZuKeId: record.peopleId }))
          }}
            userInfo={this.props.suicongInfo}
          >
            <TextEllipse><Highlighter
              highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
              searchWords={[this.props.searchText]}
              autoEscape
              textToHighlight={text.toString()}
            /></TextEllipse>
          </Personinfo>
        );
      }
      return (
        <Personinfo
          clickBefore={() => {
            this.props.dispatch(actionhelper.getUserInfo({ id: record.peopleId }))
        }}
          userInfo={this.props.peopleInfo}
        >
          <TextEllipse><Highlighter
            highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
            searchWords={[this.props.searchText]}
            autoEscape
            textToHighlight={text.toString()}
          /></TextEllipse>
        </Personinfo>
      );
    }
  }

  renderimage = (column) => {
    column.render = (text, record) => {
      const imgBox = record.fangwuPic || record.fanghaoPic
      if (!Array.isArray(imgBox) || imgBox.length === 0) {
        return '无'
      }
      return (
        <ImgLun imgBox={imgBox}>
          查看
        </ImgLun>

      );
    }
  }

  render() {
    const { dataSource, expendData, refresh, currentStar } = this.props;
    console.log('refresh: ', expendData, refresh)
    return (
      <div>
        <BackTop />
        <Row type="flex" justify="space-between" >
          <Col span={4} >
            <Select
              showSearch
              style={{ minWidth: '120px' }}
              placeholder="选择房屋安全等级"
              onChange={this.handleChange}
              value={String(currentStar)}
            >
              <Option value="1">不合格</Option>
              <Option value="2">基本合格</Option>
              <Option value="3">合格</Option>
              <Option value="4">良好</Option>
              <Option value="5">优秀</Option>
            </Select>
          </Col>
          <Col span={2} >
            <Button style={{ float: 'right' }} onClick={this.excelDownload} >Excel</Button>
          </Col>
        </Row>
        <div style={{ height: '12px' }} />
        <Spin spinning={this.props.loading}>
          <Table
            columns={this.getColumns(constColumns)}
            dataSource={dataSource}
            pagination={{
              size: 'small',
              pageSize: 20,
              pageSizeOptions: ['10', '20', '50', '100', '200'],
              showSizeChanger: true,
              showQuickJumper: true
            }}
            size="small"
            rowKey="id"
          />
        </Spin>
      </div>);
  }
}

export const mapStateToProps = (state) => {
  const {
    problemRoomDataSource,
    originProblemRoomDataSource,
    peopleInfo,
    currentStar
  } = state[COMMON];
  const effects = state.loading.effects
  return {
    originDataSource: originProblemRoomDataSource,
    dataSource: problemRoomDataSource,
    peopleInfo,
    currentStar,
    loading: !!effects['COMMON/getProblemRoom'],
  }
}

export default connect(mapStateToProps)(HouseTable);
