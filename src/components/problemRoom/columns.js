export const houseType = {
  '0': '单元楼、公寓楼',
  '1': '筒子楼',
  '2': '别墅',
  '3': '自建楼',
  '4': '平房',
  '5': '四合院',
  '6': '活动房',
  '7': '车库',
  '8': '杂物间',
  '9': '其他'
}


export const columns = [
  {
    title: '房屋地址',
    dataIndex: 'address',
    key: 'address',
    visible: true,
    searchable: true
  },
  {
    title: '面积',
    dataIndex: 'area',
    key: 'area',
    visible: true,
    // renderFn: 'renderLink',
  },
  {
    title: '总出租间数',
    dataIndex: 'chuZuJianshu',
    key: 'chuZuJianshu',
    visible: true
    // renderFn: 'renderLink',
  },
  {
    title: '已出租间数',
    dataIndex: 'yizhu',
    key: 'yizhu',
    visible: true
  },
  {
    title: '房屋类型',
    dataIndex: 'fangwuType',
    key: 'fangwuType',
    visible: true,
    renderFn: 'renderType',
  },
  {
    title: '备注',
    dataIndex: 'comment',
    key: 'comment',
    visible: true,
    // renderFn: 'renderLink',
  },
  {
    title: '房东信息',
    dataIndex: 'fangdonginfo',
    key: 'fangdonginfo',
    visible: true,
    renderFn: 'renderInfoModal'
  },
  {
    title: '房屋照片',
    dataIndex: 'baozhengshupath',
    key: 'baozhengshupath',
    visible: true,
    renderFn: 'renderimage',
  },
  {
    title: '房屋评级',
    dataIndex: 'star',
    key: 'star',
    visible: true,
    renderFn: 'renderStar',
  },
]
