import React, { Component } from 'react';
import { Modal } from 'antd';

// const pb = {
//   0: '消防设施问题',
//   1: '安全防范情况问题',
//   2: '存在违禁品',
//   3: '其他问题'
// }

class ResultModal extends Component {
    state = { visible: false }

  showModal = () => {
    this.setState({
      visible: true,
    });
  }

  handleOk = (e) => {
    console.log(e);
    this.setState({
      visible: false,
    });
  }

  handleCancel = (e) => {
    console.log(e);
    this.setState({
      visible: false,
    });
  }
  render() {
    let { fangwuPic = [], problem, detail, children } = this.props;
    return (
      <div>
        <div>
          <a onClick={this.showModal} style={{ textDecoration: 'underline' }} >{children}</a>
          <Modal
            title="检查结果"
            visible={this.state.visible}
            onOk={this.handleOk}
            onCancel={this.handleCancel}
            footer={null}
          >
            <p>{problem.split(',').map((ele, i) => {
                return (i + 1) + '. ' + ele + ';'
            })}</p>
            <p>{detail}</p>
            {
                fangwuPic.map((ele, i) =>
                (<div key={i} >
                  <img
                    style={{ width: '100%', minHeight: '400px' }}
                    alt="pic"
                    src={ele}
                  />
                </div>))
               }
          </Modal>
        </div>
      </div>
    );
  }
}


export default ResultModal;
