import React, { Component } from 'react'
import { DatePicker } from 'antd';
import moment from 'moment'
import { connect } from 'dva';
import locale from 'antd/lib/date-picker/locale/zh_CN';
import { PEOPLE } from '../../utils/NameSpace';
import { isNoData } from '../../utils/Util'
import actionhelper from '../../action/people';


const dateFormat = 'MM/DD/YYYY'


class PeopleDate extends Component {
  state = {
    // endOpen: false,
  };
  onDateChange = (field, value) => {
    this.props.dispatch(actionhelper.setData({ [field]: value }))
  }
  onStartChange = (value) => {
    this.onDateChange('startValue', value);
  }

  onEndChange = (value) => {
    this.onDateChange('endValue', value);
  }
  disabledStartDate = (startValue) => {
    const endValue = this.props.endValue;
    if (!startValue || !endValue) {
      return false;
    }
    return startValue.valueOf() > endValue.valueOf();
  }

  disabledEndDate = (endValue) => {
    const startValue = this.props.startValue;
    if (!endValue || !startValue) {
      return false;
    }
    return endValue.valueOf() <= startValue.valueOf();
  }


  //   handleStartOpenChange = (open) => {
  //     if (!open) {
  //       this.setState({ endOpen: true });
  //     }
  //   }

  //   handleEndOpenChange = (open) => {
  //     this.setState({ endOpen: open });
  //   }

  render() {
    const { startValue, endValue } = this.props;
    // const { endOpen } = this.state;
    let from = !isNoData(startValue) ? moment(startValue) : null
    let to = !isNoData(endValue) ? moment(endValue) : null
    return (
      <div style={{ float: 'right' }} >
        <DatePicker
          disabledDate={this.disabledStartDate}
          showTime
          format={dateFormat}
          value={from}
          placeholder="开始录入时间"
          onChange={this.onStartChange}
          onOpenChange={this.handleStartOpenChange}
          locale={locale}
        />
        <span>~</span>
        <DatePicker
          disabledDate={this.disabledEndDate}
          showTime
          format={dateFormat}
          value={to}
          placeholder="结束录入时间"
          onChange={this.onEndChange}
          locale={locale}
        //   open={endOpen}
        //   onOpenChange={this.handleEndOpenChange}
        />
      </div>);
  }
}

export const mapStateToProps = (state) => {
  const { dataSource, currentLevel, startValue, endValue } = state[PEOPLE];
  return {
    dataSource,
    currentLevel,
    startValue,
    endValue
  }
}

export default connect(mapStateToProps)(PeopleDate);
