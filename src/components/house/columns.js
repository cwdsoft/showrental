export const houseType = {
  '0': '单元楼、公寓楼',
  '1': '筒子楼',
  '2': '别墅',
  '3': '自建楼',
  '4': '平房',
  '5': '四合院',
  '6': '活动房',
  '7': '车库',
  '8': '杂物间',
  '9': '其他'
}


export const columns = [
  {
    title: '房屋地址',
    dataIndex: 'address',
    key: 'address',
    visible: true,
    searchable: true
  },
  {
    title: '面积',
    dataIndex: 'area',
    key: 'area',
    visible: true,
    // renderFn: 'renderLink',
  },
  {
    title: '总出租间数',
    dataIndex: 'chuZuJianshu',
    key: 'chuZuJianshu',
    visible: true
    // renderFn: 'renderLink',
  },
  {
    title: '已出租间数',
    dataIndex: 'yizhu',
    key: 'yizhu',
    visible: true
  },
  {
    title: '房屋类型',
    dataIndex: 'fangwuType',
    key: 'fangwuType',
    visible: true,
    renderFn: 'renderType',
  },
  {
    title: '备注',
    dataIndex: 'comment',
    key: 'comment',
    visible: true,
    // renderFn: 'renderLink',
  },
  {
    title: '房东信息',
    dataIndex: 'fangdonginfo',
    key: 'fangdonginfo',
    visible: true,
    renderFn: 'renderInfoModal'
  },
  {
    title: '房屋照片',
    dataIndex: 'baozhengshupath',
    key: 'baozhengshupath',
    visible: true,
    renderFn: 'renderimage',
  },
  {
    title: '房屋评级',
    dataIndex: 'star',
    key: 'star',
    visible: true,
    renderFn: 'renderStar',
  },
]

export const roomcolumns = [
  {
    title: '房间号',
    dataIndex: 'fangHaoName',
    key: 'fangHaoName',
    visible: true,
  },
  {
    title: '入住日期',
    dataIndex: 'inDate',
    key: 'inDate',
    visible: true,
  },
  {
    title: '当前租客',
    dataIndex: 'name',
    key: 'name',
    visible: true,
    renderFn: 'renderInfoModal'
  },
  {
    title: '随从人数',
    dataIndex: 'suizhuCount',
    key: 'suizhuCount',
    visible: true,
    renderFn: 'renderInfoModal'
  },
  {
    title: '房屋照片',
    dataIndex: 'fanghaoPic',
    key: 'fanghaoPic',
    visible: true,
    renderFn: 'renderimage',
  },
  // {
  //   title: '安全等级',
  //   dataIndex: 'safeLevel',
  //   key: 'safeLevel',
  //   visible: true,
  //   renderFn: 'renderSafe'
  // },
]
