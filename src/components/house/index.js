import React, { Component } from 'react'
import ExportJsonExcel from 'js-export-excel'
import {
  Table, Input, BackTop, Spin, Row, Col, Button
  // Badge
} from 'antd';
import { connect } from 'dva';
import Highlighter from 'react-highlight-words';
import { HOUSE } from '../../utils/NameSpace';
import { isNoData } from '../../utils/Util'
import css from './index.css'
import TextEllipse from '../common/textEllipse';
import ImgLun from '../common/imgLun';
import Personinfo from '../common/personinfo';
import { columns as constColumns, roomcolumns, houseType } from './columns.js'
import actionhelper from '../../action/house';
import { getExcelData } from '../utils'

const Search = Input.Search;


class HouseTable extends Component {
  getColumns = (columns) => {
    const TableColumns = columns.filter(ele => ele.visible).map((item) => {
      // if (!isNoData(item.sortFn)) { this[item.sortFn](item, item.dataIndex) }
      if (item.filterable) { this.filterColumn(item, item.dataIndex) }
      if (isNoData(item.renderFn)) { this.renderColumn(item); return item; }
      this[item.renderFn](item, item.dataIndex);
      return item;
    });
    return TableColumns;
  }

  excelDownload = () => {
    const { dataSource } = this.props;
    let columns = this.getColumns(constColumns)
    let ds = dataSource

    let option = {};
    option.fileName = '房屋表'
    option.datas = getExcelData(columns, ds)
    let toExcel = new ExportJsonExcel(option);
    toExcel.saveExcel();
  }
  filterColumn = (column, dataIndex) => {
    const { dataSource } = this.props;
    let Box = new Set();
    dataSource.forEach((element) => {
      if (isNoData(element[dataIndex])) return;
      Box.add(element[dataIndex])
    });
    column.filters = Array.from(Box, (ele) => { return { text: ele, value: ele } })
    column.onFilter = (value, record) => {
      if (isNoData(record[dataIndex])) return false;
      return record[dataIndex].toString() === value.toString()
    }
  }

  doOtherRow = (record, ed) => {
    let expendData = []
    console.log('expend: ', ed[record.id])
    if (ed) {
      expendData = ed[record.id]
    }
    return (
      <div>
        <Spin spinning={this.props.roomLoading}>
          <Table
            columns={this.getColumns(roomcolumns)}
            dataSource={expendData}
            pagination={false}
            rowClassName={
            (rd) => {
              if (rd.safeLevel === 1) {
                return css.yellowLine
              } else if (rd.safeLevel === 2) {
                return css.redLine
              }
              return css.greenLine
            }
          }
          />
        </Spin>
      </div>
    );
  };

  expendChange = (expanded, record) => {
    // const { dispatch } = this.props;
    if (expanded === false) {
      console.log('合并！');
    } else {
      console.log('展开！');
      if (this.props.expendData[record.id]) {
        return
      }
      this.props.dispatch(actionhelper.getRoom({
        fangwuid: record.id
      }))
    }
  }

  searchAll = (value) => {
    const { originDataSource, dispatch } = this.props;
    if (isNoData(value)) {
      dispatch(actionhelper.setData({ dataSource: originDataSource, expendData: [] }))
    }
    let fiDataSource = originDataSource.filter((ele) => {
      let lock = false;
      if (String(ele.address).includes(value)) {
        lock = true;
      }
      if (String(ele.area).includes(value)) {
        lock = true;
      }
      if (String(ele.chuZuJianshu).includes(value)) {
        lock = true;
      }
      if (String(ele.yizhu).includes(value)) {
        lock = true;
      }
      if (String(ele.fangwuType).includes(value)) {
        lock = true;
      }
      if (String(ele.comment).includes(value)) {
        lock = true;
      }
      if (String(ele.fangdonginfo).includes(value)) {
        lock = true;
      }
      return lock
    })
    dispatch(actionhelper.setData({
      dataSource: fiDataSource,
      expendData: [],
      searchText: value
    }))
  }

  renderColumn = (column) => {
    column.render = (text) => {
      if (!text) {
        return text
      }
      return (
        <TextEllipse><Highlighter
          highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
          searchWords={[this.props.searchText]}
          autoEscape
          textToHighlight={text.toString()}
        /></TextEllipse>
      );
    }
  }
  renderType = (column) => {
    column.render = (text, record) => {
      let txt = houseType[record.fangWuType]
      if (!txt) {
        return txt
      }
      return (
        <TextEllipse>{txt}</TextEllipse>
      );
    }
  }
  renderStar = (column) => {
    column.render = (text, record) => {
      let count = Array.from(Array(record.star), () => 1);
      return (
        <div>
          {
          count.map((ele, i) => <img key={i} style={{ width: '20px', height: '20px' }} alt="star" src="./loc/image/star.png" />)
        }
        </div>
      );
    }
  }
  // renderSafe = (column) => {
  //   column.render = (text, record) => {
  //     return (
  //       <TextEllipse >{ record.safeLevel}</TextEllipse>
  //     );
  //   }
  // }
  renderInfoModal = (column) => {
    column.render = (text, record) => {
      if (column.dataIndex === 'suizhuCount') {
        if (String(text) === '0') {
          return text
        }
        if (!text) {
          return text
        }
        return (
          <Personinfo
            clickBefore={() => {
              this.props.dispatch(actionhelper.getAllSuicong({ ZuKeId: record.peopleId }))
          }}
            userInfo={this.props.suicongInfo}
          >
            <TextEllipse><Highlighter
              highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
              searchWords={[this.props.searchText]}
              autoEscape
              textToHighlight={text.toString()}
            /></TextEllipse>
          </Personinfo>
        );
      }
      return (
        <Personinfo
          clickBefore={() => {
            this.props.dispatch(actionhelper.getUserInfo({ id: record.peopleId }))
        }}
          userInfo={this.props.peopleInfo}
        >
          <TextEllipse><Highlighter
            highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
            searchWords={[this.props.searchText]}
            autoEscape
            textToHighlight={text.toString()}
          /></TextEllipse>
        </Personinfo>
      );
    }
  }

  renderimage = (column) => {
    column.render = (text, record) => {
      const imgBox = record.fangwuPic || record.fanghaoPic
      if (!Array.isArray(imgBox) || imgBox.length === 0) {
        return '无'
      }
      return (
        <ImgLun imgBox={imgBox}>
          查看
        </ImgLun>

      );
    }
  }
  render() {
    const { dataSource, expendData, refresh, searchText } = this.props;
    console.log('refresh: ', expendData, refresh)
    return (
      <div>
        <BackTop />
        <Row type="flex" justify="space-between" >
          <Col span={6} >
            <Search
              placeholder="查询字段"
              value={searchText}
              onChange={(e) => { this.searchAll(e.target.value) }}
              onSearch={this.searchAll}
              enterButton
            />
          </Col>
          <Col span={2} >
            <Button style={{ float: 'right' }} onClick={this.excelDownload} >Excel</Button>
          </Col>
        </Row>
        <div style={{ height: '12px' }} />
        <Spin spinning={this.props.loading}>
          <Table
            columns={this.getColumns(constColumns)}
            expandedRowRender={record => this.doOtherRow(record, expendData)}
            onExpand={this.expendChange}
            dataSource={dataSource}
            pagination={{
              size: 'small',
              pageSize: 20,
              pageSizeOptions: ['10', '20', '50', '100', '200'],
              showSizeChanger: true,
              showQuickJumper: true
            }}
            size="small"
            rowKey="id"
          />
        </Spin>
      </div>);
  }
}

export const mapStateToProps = (state) => {
  const {
    originDataSource,
    dataSource,
    expendData,
    refresh,
    peopleInfo,
    suicongInfo,
    searchText
  } = state[HOUSE];
  const effects = state.loading.effects
  return {
    originDataSource,
    dataSource,
    expendData,
    peopleInfo,
    refresh,
    suicongInfo,
    roomLoading: !!effects['HOUSE/getRoom'],
    loading: !!effects['HOUSE/getInit'],
    searchText
  }
}

export default connect(mapStateToProps)(HouseTable);
