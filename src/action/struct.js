import { actionsFactory } from '../utils/Util';
import { STRUCT } from '../utils/NameSpace';

const action = {
  GET_TREE: 'getTree',
  GET_USER_INFO: 'getUserInfo',
  SET_DATA: 'setData',
  SET_LOADING: 'setLoading',
}

export const Actions = actionsFactory(action, STRUCT);

export default Actions
