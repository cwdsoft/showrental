import { actionsFactory } from '../utils/Util';
import { PEOPLE } from '../utils/NameSpace';

const action = {
  GET_INIT: 'getInit',
  SET_DATA: 'setData',
  SET_LOADING: 'setLoading',
  MARK_CHANGE: 'markChange',
}

export const Actions = actionsFactory(action, PEOPLE);

export default Actions
