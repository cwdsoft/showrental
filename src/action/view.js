import { actionsFactory } from '../utils/Util';
import { VIEW } from '../utils/NameSpace';

const action = {
  GET_INIT: 'getInit',
  GET_JIXIAO: 'getJixiao',
  GET_AllHOUSE_POSI: 'getAllhousePosi',
  SET_DATA: 'setData',
  SET_LOADING: 'setLoading',
}

export const Actions = actionsFactory(action, VIEW);

export default Actions
