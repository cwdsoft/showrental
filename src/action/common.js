import { actionsFactory } from '../utils/Util';
import { COMMON } from '../utils/NameSpace';

const action = {
  GET_INIT: 'getInit',
  GET_TREE: 'getTree',
  GET_ROOM_TREE: 'getRoomTree',
  GET_REN_PIE: 'getRenPie',
  GET_SEX_PIE: 'getSexPie',
  SET_DATA: 'setData',
  GET_USER_INFO: 'getUserInfo',
  GET_USER_PATH: 'getUserPath',
  GET_ROOM_CHECK_LOG: 'getRoomCheckLog',
  GET_PROBLEM_ROOM: 'getProblemRoom',
  GET_COLOR_PEOPLE: 'getColorPeople',
  SET_LOADING: 'setLoading',

  SEND_NEWS: 'sendNews',
  SEND_MSG: 'sendMsg',
  GET_BASE_INFO: 'getBaseInfo',

  CHECK_LOGIN: 'checkLogin',

  GET_FANGHAO_TREE: 'getFanghaoTree',
  GET_MJ_WGY_TREE: 'getMjWgyTree',

  GET_KAOHE_LOG: 'getKaoheLog',
  GET_MY_INFO: 'getMyInfo',

  UPDATE_FILE: 'updateFile',

  SEARCH_FACE: 'searchFace',

  GET_NEWS: 'getNews',
  SAVE_NEWS: 'saveNews',
}

export const Actions = actionsFactory(action, COMMON);

export default Actions
