import { actionsFactory } from '../utils/Util';
import { HOUSE } from '../utils/NameSpace';

const action = {
  GET_INIT: 'getInit',
  GET_ROOM: 'getRoom',
  GET_USER_INFO: 'getUserInfo',
  GET_ALL_SUICONG: 'getAllSuicong',
  SET_DATA: 'setData',
  SET_LOADING: 'setLoading',
}

export const Actions = actionsFactory(action, HOUSE);

export default Actions
