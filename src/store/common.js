const store = {
  isLogin: false,
  selectedKeys: 'VIEW',
  projectName: '出租屋旅店式管理系统可视化界面',
  duizhao: {
    VIEW: [{ path: 'VIEW', title: '总览' }],
    PATHANLY: [{ path: 'anly', title: '分析' }, { path: 'PATHANLY', title: '轨迹分析' }],
    FACEANLY: [{ path: 'anly', title: '分析' }, { path: 'FACEANLY', title: '人脸分析' }],
    STRUCT: [{ path: 'anly', title: '分析' }, { path: 'STRUCT', title: '人员结构' }],
    TJ: [{ path: 'anly', title: '分析' }, { path: 'TJ', title: '统计分析' }],
    HOUSE: [{ path: 'HOUSE', title: '房屋' }],
    PEOPLE: [{ path: 'PEOPLE', title: '人员' }],
    CHECKLOG: [{ path: 'CHECK', title: '检查' }, { path: 'CHECKLOG', title: '日常检查记录' }],
    PROBLEMROOM: [{ path: 'CHECK', title: '检查' }, { path: 'PROBLEMROOM', title: '房屋安全等级' }],
    THRIRECOLOR: [{ path: 'CHECK', title: '检查' }, { path: 'THRIRECOLOR', title: '承租人三色预警' }],
    PERFORMANCELOG: [{ path: 'PERFORMANCE', title: '绩效' }, { path: 'PERFORMANCELOG', title: '网格员绩效记录' }],
    NEWS: [{ path: 'SEND', title: '发布' }, { path: 'NEWS', title: '新闻公告' }],
    SYSTEMMSG: [{ path: 'SEND', title: '发布' }, { path: 'SYSTEMMSG', title: '系统消息' }],
    // NEWSMANA: [{ path: 'SEND', title: '发布' }, { path: 'NEWSMANA', title: '管理' }],
  },
  itemBox: [
    {
      name: 'VIEW',
      title: '总览',
      key: 'VIEW',
      icon: 'pie-chart'
    },
    {
      name: 'anly',
      title: '分析',
      key: 2,
      icon: 'border-inner',
      children: [
        {
          key: 'PATHANLY',
          title: '轨迹分析'
        },
        {
          key: 'FACEANLY',
          title: '人脸分析'
        },
        {
          key: 'STRUCT',
          title: '人员结构'
        },
        {
          key: 'TJ',
          title: '统计分析'
        },
      ]
    },
    {
      name: 'HOUSE',
      title: '房屋',
      key: 'HOUSE',
      icon: 'home',
    },
    {
      name: 'PEOPLE',
      title: '人员',
      key: 'PEOPLE',
      icon: 'team',
    },
    {
      name: 'CHECK',
      title: '检查',
      key: 'CHECK',
      icon: 'reconciliation',
      children: [
        {
          key: 'CHECKLOG',
          title: '日常检查记录'
        },
        {
          key: 'PROBLEMROOM',
          title: '房屋安全等级'
        },
        {
          key: 'THRIRECOLOR',
          title: '承租人三色预警'
        },
      ]
    },
    {
      name: 'PERFORMANCE',
      title: '绩效',
      key: 'PERFORMANCE',
      icon: 'fire',
      children: [
        {
          key: 'PERFORMANCELOG',
          title: '网格员绩效记录'
        },
      ]
    },
    {
      name: 'SEND',
      title: '发布',
      key: 'SEND',
      icon: 'message',
      children: [
        {
          key: 'NEWS',
          title: '新闻公告'
        },
        {
          key: 'SYSTEMMSG',
          title: '系统消息'
        },
        // {
        //   key: 'NEWSMANA',
        //   title: '管理'
        // },
      ]
    },
  ],
  sexPie: {},
  renPie: [],
  peopleInfo: [],
  tree: [],
  userPath: [],
  roomTree: [],
  roomLogSource: [],
  problemRoomDataSource: [],
  originProblemRoomDataSource: [],
  currentStar: 1,
  currentPeopleClass: '严管类',
  originColorPeopleDataSource: [],
  ColorPeopleDataSource: [],
  // problem room
  safeLevel: '1',
  hasUserInfo: false,
  userInfo: {},

  // checklog
  fanghaoTree: [],
  fangWuList: [],
  mjWgyTree: [],
  jixiaoLogSource: [],
  jixiaoexpendData: [],
  searchData: [],
  originPic: '',
  newsList: [],
  baseInfo: {}
}

export default store
