
// when build to production publicPath should be set as 'RIT-Mark'

export default Object.assign({}, {
  entry: './src/index.js',
  disableCSSModules: false,
  publicPath: '/',
  outputPath: './dist',
  extraBabelPlugins: [
    ['import', { 'libraryName': 'antd','libraryDirectory': 'es','style': true }],
  ],
  externals: {
    "axios": "axios",
    "Promise": "Promise"
  },
  extraPostCSSPlugins: [],
  env: {
    development: {
      extraBabelPlugins: [
        'dva-hmr'
      ]
    }
  },
  es5ImcompatibleVersions: true,
  // hash: true,
})