/* eslint-env browser */
/* eslint-disable */
// import { getCookieByName, setCookie, NotificationUtil } from './util';

// axios cames from script in html

if (!(window && window.Promise)) {
    window.Promise = Promise;
  }
  
  window.Ajax = function (option) {
    var Notification = option.Notification;
    // var setCookie = option.setCookie;
    // var getCookieByName = option.getCookieByName;
    var queryString = option.queryString;
    // var devEnv = option.devEnv;
  
    var defaultConfig = {
      headers: {},
    //   errorNotification: false,
    //   successNotification: false
    };
  
    // axios.defaults.headers.locale = window.languageEnvironment;
    // axios.defaults.headers.currentUserRole = window.currentUserRole;
    defaultConfig.headers['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';    
   defaultConfig.headers["Access-Control-Allow-Origin"]="*";
   defaultConfig.headers["Access-Control-Allow-Headers"]="X-Requested-With,Content-Type";
   defaultConfig.headers["Access-Control-Allow-Methods"]="PUT,POST,GET,DELETE,OPTIONS";

    defaultConfig.timeout = 50000;

    function errorCheck(response) {
      Notification('error', { message: '出现错误', duration: 0, description: response.message });
    }
    function checkStatus(response) {
      return response.data
    }
    function request(config) {
      config = Object.assign({}, defaultConfig, config);
      if (!config.noStringify) {
        // config.headers['Authorization'] = storage.getItem('openid');
        config.data = JSON.stringify(config.data);
      }
      return axios.request(config).then(checkStatus).catch(errorCheck);
    }
  
    function encodeQuery(url) {
      var parsed = queryString.parseUrl(url);
      var parsedQueryString = queryString.stringify(parsed.query);
      if (!parsedQueryString) return url;
      return parsed.url + '?' + parsedQueryString;
    }
  
    function GET(url, data, config) {
      var CONF = {
        url: queryString ? encodeQuery(url) : url,
        method: 'GET',
        data: data
      };
      config = Object.assign({ headers: {} }, CONF, config);
      // disable IE ajax request caching
      const openid = window.sessionStorage.getItem('openid');
      config.headers['Authorization'] = openid;
      config.headers['Cache-Control'] = 'no-cache';
      config.headers['Pragma'] = 'no-cache';
      return request(config);
    }
  
    function POST(url, data, config) {
      config = config || {};
      config.headers = config.headers ? config.headers : {};
      const openid = window.sessionStorage.getItem('openid');
      config.headers['Authorization'] = openid;
      return axios.post.call(this, url, data, config).then(checkStatus).catch(errorCheck);
    }
  
    function PUT(url, data, config) {
      data = data || {};
      var CONF = {
        url: url,
        method: 'PUT',
        data: data,
        headers: {
          'Content-Type': 'application/json; charset=UTF-8'
        }
      };
      return request(Object.assign({}, CONF, config));
    }
  
    function DELETE(url, config) {
      var CONF = {
        url: url,
        method: 'DELETE'
      };
      return request(Object.assign({}, CONF, config));
    }
  
    this.request = request;
    this.PUT = PUT;
    this.GET = GET;
    this.POST = POST;
    this.DELETE = DELETE;
  };
  